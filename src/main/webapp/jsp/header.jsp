<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>

<html>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <c:if test="${sessionScope.User.getUserStatus() ne 'BLOCKED'}">
                <li class="nav-item active">
                    <c:choose>
                        <c:when test="${sessionScope.User.getUserRole() eq 'ADMIN'}">
                            <form class="form-inline my-2 my-lg-0">
                                <button class="btn btn-link" type="submit" formaction="/adminProfile" formmethod="get"><ctg:locale message="AdminProfile"/></button>
                            </form>
                        </c:when>
                        <c:when test="${sessionScope.User.getUserRole() eq 'MANAGER'}">
                            <form class="form-inline my-2 my-lg-0">
                                <button class="btn btn-link" type="submit" formaction="/managerProfile" formmethod="get"><ctg:locale message="ManagerProfile"/></button>
                            </form>
                        </c:when>
                        <c:when test="${sessionScope.User.getUserRole() eq 'CLIENT'}">
                            <form class="form-inline my-2 my-lg-0">
                                <button class="btn btn-link" type="submit" formaction="/userProfile" formmethod="get"><ctg:locale message="ClientProfile"/></button>
                            </form>
                        </c:when>
                        <c:otherwise>
                            <a class="nav-link disabled" href="${pageContext.request.contextPath}/jsp/login.jsp"><ctg:locale message="LogIn"/></a>
                        </c:otherwise>
                    </c:choose>
                </li>
            </c:if>

            <li class="nav-item active">
                <c:choose>
                    <c:when test="${User==null}">
                        <a class="nav-link disabled" href="${pageContext.request.contextPath}/jsp/registration.jsp"><ctg:locale message="Registration"/></a>
                    </c:when>
                </c:choose>
            </li>

            <c:choose>
                <c:when test="${User==null}">
                    <a class="nav-link disabled" href="#"><ctg:locale message="UserRegisterInfo"/></a>
                </c:when>
                <c:otherwise>
                    <li class="nav-item">
                        <a class="nav-link disabled" href="#"><ctg:locale message="UserInfo"/> ${sessionScope.User.getUserName()} </a>
                    </li>
                </c:otherwise>
            </c:choose>
        </ul>

        <form class="form-inline my-2 my-lg-0">
            <select class="custom-select" name = "lang" required>
                <option value="en">English</option>
                <option value="ru">Russian</option>
                <option selected ="selected"></option>
            </select>
            <button class="btn btn-link my-2 my-sm-0" type="submit" formaction="/changeLocal" formmethod="get"><ctg:locale message="changeLang"/></button>
        </form>

        <c:if test="${User!=null}">
            <form class="form-inline my-2 my-lg-0">
                <button class="btn btn-outline-primary my-2 my-sm-0" type="submit" formaction="/logout"
                        formmethod="post"><ctg:locale message="LogoutButton"/></button>
            </form>
        </c:if>
    </div>
</nav>
<script type="text/javascript" src="../libs/jquery-3.3.1/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="../libs/propper-1.11.0/popper.min.js"></script>
<script type="text/javascript" src="../libs/bootstrap-4.1.3/js/bootstrap.min.js"></script>
</html>