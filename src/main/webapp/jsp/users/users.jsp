<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="libs/bootstrap-4.1.3/css/bootstrap.min.css">
    <title><ctg:locale message="UserList"/></title>
</head>
<body>
<jsp:include page="/jsp/header.jsp"/>
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-md-auto">
            <h4><ctg:locale message="UserList"/></h4>
        </div>
    </div>

    <c:set var="userList" value='${requestScope["userList"]}' />

    <table class="table table-striped table-bordered table-sm" cellspacing="0">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col"><ctg:locale message="FirstName"/></th>
            <th scope="col"><ctg:locale message="LastName"/></th>
            <th scope="col"><ctg:locale message="UserName"/></th>
            <th scope="col"><ctg:locale message="Email"/></th>
            <th scope="col"><ctg:locale message="Role"/></th>
            <th scope="col"><ctg:locale message="Status"/></th>
            <th scope="col"><ctg:locale message="View"/></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${userList}" var="user" varStatus="status">
            <tr>
                <td>${user.getUserId()}</td>
                <td>${user.getFirstName()}</td>
                <td>${user.getLastName()}</td>
                <td>${user.getUserName()}</td>
                <td>${user.getEmail()}</td>
                <td>${user.getUserRole()}</td>
                <td>${user.getUserStatus()}</td>
                <td><a href="/users/${user.getUserId()}"><ctg:locale message="View"/></a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

    <a href="/adminProfile"><ctg:locale message="Menu"/></a><br>

    <script type="text/javascript" src="libs/jquery-3.3.1/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="libs/propper-1.11.0/popper.min.js"></script>
    <script type="text/javascript" src="libs/bootstrap-4.1.3/js/bootstrap.min.js"></script>
</div>
</body>
</html>
