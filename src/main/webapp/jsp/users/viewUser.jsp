<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/libs/bootstrap-4.1.3/css/bootstrap.min.css">
    <title><ctg:locale message="ViewUser"/></title>
</head>
<body>
<jsp:include page="/jsp/header.jsp"/>
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-md-auto">
            <h4><ctg:locale message="ViewUser"/></h4>
        </div>
    </div>

    <c:set var="userDTO" value='${requestScope["userDTO"]}' />

    <form action="/viewUser" method="get">
        <input name="userId" type="hidden" class="form-control" id="id" value=${userDTO.getUserId()} >
        <div class="form-group row">
            <label for="firstName" class="col-sm-2 col-form-label"><ctg:locale message="FirstName"/></label>
            <div class="col-sm-2">
                <input name="firstName" type="text" class="form-control" id="firstName" readonly value=${userDTO.getFirstName()} >
            </div>
        </div>
        <div class="form-group row">
            <label for="lastName" class="col-sm-2 col-form-label"><ctg:locale message="LastName"/></label>
            <div class="col-sm-2">
                <input name="lastName" type="text" class="form-control" id="lastName" readonly value=${userDTO.getLastName()} >
            </div>
        </div>
        <div class="form-group row">
            <label for="userName" class="col-sm-2 col-form-label"><ctg:locale message="UserName"/></label>
            <div class="col-sm-2">
                <input name="userName" type="text" class="form-control" id="userName" readonly value=${userDTO.getUserName()} >
            </div>
        </div>
        <input name="password" type="hidden" class="form-control" id="password" value=${userDTO.getPassword()} >
        <div class="form-group row">
            <label for="email" class="col-sm-2 col-form-label"><ctg:locale message="Email"/></label>
            <div class="col-sm-2">
                <input name="email" type="text" class="form-control" id="email"  readonly value=${userDTO.getEmail()} >
            </div>
        </div>
        <div class="form-group row">
            <label for="userRole" class="col-sm-2 col-form-label"><ctg:locale message="UserRole"/></label>
            <div class="col-sm-2">
                <select name="userRole" class="custom-select">
                    <option selected>${userDTO.getUserRoleString()}</option>
                    <option value="CLIENT">CLIENT</option>
                    <option value="MANAGER">MANAGER</option>
                    <option value="ADMIN">ADMIN</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="userStatus" class="col-sm-2 col-form-label"><ctg:locale message="Status"/></label>
            <div class="col-sm-2">
                <select name="userStatus" class="custom-select">
                    <option selected>${userDTO.getUserStatusString()}</option>
                    <option value="ACTIVE">ACTIVE</option>
                    <option value="BLOCKED">BLOCKED</option>
                </select>
            </div>
        </div>
        <button type="submit" class="btn btn-primary" formaction="/updateUser" formmethod="post" ><ctg:locale message="Update"/></button>
        <button type="submit" class="btn btn-primary" formaction="/deleteUser" formmethod="post" ><ctg:locale message="Delete"/></button>
    </form>

    <a href="/users"><ctg:locale message="UserList"/></a><br>

    <script type="text/javascript" src="libs/jquery-3.3.1/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="libs/propper-1.11.0/popper.min.js"></script>
    <script type="text/javascript" src="libs/bootstrap-4.1.3/js/bootstrap.min.js"></script>
</div>
</body>
</html>