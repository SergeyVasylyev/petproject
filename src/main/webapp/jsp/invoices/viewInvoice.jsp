<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/libs/bootstrap-4.1.3/css/bootstrap.min.css">
    <title><ctg:locale message="ViewInvoice"/></title>
</head>
<body>
<jsp:include page="/jsp/header.jsp"/>
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-md-auto">
            <h4><ctg:locale message="ViewInvoice"/></h4>
        </div>
    </div>

    <c:set var="invoice" value='${requestScope["invoiceDTO"]}' />

    <form action="/viewInvoice" method="get">
        <input name="id" type="hidden" class="form-control" id="id" value=${invoice.getId()} >
        <input name="orderId" type="hidden" class="form-control" id="orderId" value=${invoice.getOrder().getId()}>
        <div class="form-group row">
            <label for="orderView" class="col-sm-2 col-form-label"><ctg:locale message="Order"/></label>
            <div class="col-sm-2">
                <input name="orderView" type="text" class="form-control" id="orderView" readonly value=${invoice.getOrder().toString()}>
            </div>
        </div>
        <div class="form-group row">
            <label for="sum" class="col-sm-2 col-form-label"><ctg:locale message="InvoiceSum"/></label>
            <div class="col-sm-2">
                <input name="sum" type="text" class="form-control" id="sum" readonly  value=${invoice.getSum()} placeholder="Sum">
            </div>
        </div>
        <div class="form-group row">
            <label for="type" class="col-sm-2 col-form-label"><ctg:locale message="InvoiceIssue"/></label>
            <div class="col-sm-2">
                <input name="type" type="text" class="form-control" id="type" readonly value=${invoice.isStatusIssue()}>
            </div>
        </div>
        <c:if test="${sessionScope.User.getUserRole() eq 'CLIENT' && invoice.getOrder().getStatus().toString() eq 'APPROVED'}">
            <button type="submit" class="btn btn-primary" formaction="/invoicePayed" formmethod="post" ><ctg:locale message="Pay"/></button>
        </c:if>
        <c:if test="${sessionScope.User.getUserRole() eq 'CLIENT' && invoice.getOrder().getStatus().toString() eq 'ISSUE'}">
            <button type="submit" class="btn btn-primary" formaction="/invoiceIssuePayed" formmethod="post" ><ctg:locale message="PayForIssue"/></button>
        </c:if>
        <c:if test="${sessionScope.User.getUserRole() eq 'ADMIN'}">
            <button type="submit" class="btn btn-primary" formaction="/updateInvoice" formmethod="post" ><ctg:locale message="Update"/></button>
            <button type="submit" class="btn btn-primary" formaction="/deleteInvoice" formmethod="post" ><ctg:locale message="Delete"/></button>
        </c:if>

    </form>

    <a href="/invoices"><ctg:locale message="InvoiceList"/></a><br>

    <script type="text/javascript" src="libs/jquery-3.3.1/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="libs/propper-1.11.0/popper.min.js"></script>
    <script type="text/javascript" src="libs/bootstrap-4.1.3/js/bootstrap.min.js"></script>
</div>
</body>
</html>