<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="libs/bootstrap-4.1.3/css/bootstrap.min.css">
    <title><ctg:locale message="InvoiceList"/></title>
</head>
<body>
<jsp:include page="/jsp/header.jsp"/>
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-md-auto">
            <h4><ctg:locale message="InvoiceList"/></h4>
        </div>
    </div>

    <c:set var="invoiceList" value='${requestScope["invoiceList"]}' />

    <table class="table table-striped table-bordered table-sm" cellspacing="0">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col"><ctg:locale message="Order"/></th>
                <th scope="col"><ctg:locale message="Status"/></th>
                <th scope="col"><ctg:locale message="InvoiceIssue"/></th>
                <th scope="col"><ctg:locale message="Sum"/></th>
                <th scope="col"><ctg:locale message="User"/></th>
                <th scope="col"><ctg:locale message="View"/></th>
            </tr>
        </thead>
        <tbody>
        <c:forEach items="${invoiceList}" var="invoice" varStatus="status">
            <tr>
                <td>${invoice.getId()}</td>
                <td>${invoice.getOrder()}</td>
                <td>${invoice.getOrder().getStatus()}</td>
                <td>${invoice.isStatusIssue()}</td>
                <td>${invoice.getSum()}</td>
                <td>${invoice.getOrder().getUser().toString()}</td>
                <td><a href="/invoices/${invoice.getId()}"><ctg:locale message="View"/></a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

    <c:choose>
        <c:when test="${sessionScope.User.getUserRole() eq 'ADMIN'}">
            <a href="/adminProfile"><ctg:locale message="Menu"/></a><br>
        </c:when>
        <c:when test="${sessionScope.User.getUserRole() eq 'MANAGER'}">
            <a href="/managerProfile"><ctg:locale message="Menu"/></a><br>
        </c:when>
        <c:when test="${sessionScope.User.getUserRole() eq 'CLIENT'}">
            <a href="/userProfile"><ctg:locale message="Menu"/></a><br>
        </c:when>
    </c:choose>

    <script type="text/javascript" src="libs/jquery-3.3.1/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="libs/propper-1.11.0/popper.min.js"></script>
    <script type="text/javascript" src="libs/bootstrap-4.1.3/js/bootstrap.min.js"></script>
</div>
</body>
</html>