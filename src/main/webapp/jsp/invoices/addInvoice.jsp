<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="libs/bootstrap-4.1.3/css/bootstrap.min.css">
    <title><ctg:locale message="AddInvoice"/></title>
</head>
<body>
<jsp:include page="/jsp/header.jsp"/>
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-md-auto">
            <h4><ctg:locale message="AddInvoice"/></h4>
        </div>
    </div>

    <c:set var="order" value='${requestScope["orderDTO"]}' />
    <c:set var="isIssue" value='${requestScope["isIssueOrder"]}' />
    <c:set var="validationErrorForm" value='${requestScope["validationError"]}' />

    <form action="/invoices" method="post">
        <input name="orderId" type="hidden" class="form-control" id="orderId" value=${order.getId()}>
        <div class="form-group row">
            <label for="orderView" class="col-sm-2 col-form-label"><ctg:locale message="Order"/></label>
            <div class="col-sm-4">
                <input name="orderView" type="text" class="form-control" id="orderView" readonly value=${order.toString()}>
            </div>
        </div>
        <div class="form-group row">
            <label for="sum" class="col-sm-2 col-form-label"><ctg:locale message="Sum"/></label>
            <div class="col-sm-2">
                <input name="sum" type="text" class="form-control" id="sum"
                <c:if test="${order.getStatus().toString() ne 'PAYED'}"> readonly </c:if>
                        value=${order.getSum()} >
            </div>
        </div>
        <input name="isIssue" type="hidden" class="form-control" id="isIssue" value=${isIssue}>

        <div class="form-group row">
            <label class="col-sm-10 col-form-label text-danger">${validationErrorForm}</label>
        </div>

        <c:if test="${isIssue eq 'true'}">
            <button type="submit" class="btn btn-primary" formaction="/invoices" formmethod="post" ><ctg:locale message="CreateInvoiceIssue"/></button>
        </c:if>
        <c:if test="${isIssue ne 'true'}">
            <button type="submit" class="btn btn-primary" formaction="/invoices" formmethod="post" ><ctg:locale message="CreateInvoice"/></button>
        </c:if>
    </form>

    <a href="/invoices"><ctg:locale message="InvoiceList"/></a><br>

    <script type="text/javascript" src="libs/jquery-3.3.1/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="libs/propper-1.11.0/popper.min.js"></script>
    <script type="text/javascript" src="libs/bootstrap-4.1.3/js/bootstrap.min.js"></script>
</div>
</body>
</html>