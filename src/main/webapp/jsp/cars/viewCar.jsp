<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/libs/bootstrap-4.1.3/css/bootstrap.min.css">
    <title><ctg:locale message="ViewCar"/></title>
</head>
<body>
<jsp:include page="/jsp/header.jsp"/>
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-md-auto">
            <h4><ctg:locale message="ViewCar"/></h4>
        </div>
    </div>

    <c:set var="car" value='${requestScope["carDTO"]}' />

    <form action="/viewCar" method="get">
        <input name = "id" type="hidden" class="form-control" id="id" value=${car.getId()}>
            <div class="form-group row">
                <label for="number" class="col-sm-2 col-form-label"><ctg:locale message="Number"/></label>
                <div class="col-sm-2">
                    <input name = "number" type="text" class="form-control" id="number" value=${car.getNumber()} placeholder="Number">
                </div>
            </div>
            <div class="form-group row">
                <label for="type" class="col-sm-2 col-form-label"><ctg:locale message="Type"/></label>
                <div class="col-sm-2">
                    <input name = "type" type="text" class="form-control" id="type" value=${car.getType()} placeholder="Type">
                </div>
            </div>
            <div class="form-group row">
                <label for="quality" class="col-sm-2 col-form-label"><ctg:locale message="Quality"/></label>
                <div class="col-sm-2">
                    <input name = "quality" type="text" class="form-control" id="quality" value=${car.getQuality()} placeholder="Quality">
                </div>
            </div>
            <div class="form-group row">
                <label for="name" class="col-sm-2 col-form-label"><ctg:locale message="Name"/></label>
                <div class="col-sm-2">
                    <input name = "name" type="text" class="form-control" id="name" value=${car.getName()} placeholder="Name">
                </div>
            </div>
            <div class="form-group row">
                <label for="rentPrice" class="col-sm-2 col-form-label"><ctg:locale message="RentPrice"/></label>
                <div class="col-sm-2">
                    <input name="rentPrice" type="text" class="form-control" id="rentPrice" value=${car.getRentPrice()}>
                </div>
            </div>
            <div class="form-group row">
                <label for="available" class="col-sm-2 col-form-label"><ctg:locale message="Available"/></label>
                <div class="col-sm-2">
                    <input name="available" type="text" class="form-control" id="available" value=${car.isAvailable()} >
                </div>
            </div>

        <c:if test="${sessionScope.User.getUserRole() eq 'ADMIN'}">
            <button type="submit" class="btn btn-primary" formaction="/updateCar" formmethod="post" ><ctg:locale message="Update"/></button>
            <button type="submit" class="btn btn-primary" formaction="/deleteCar" formmethod="post" ><ctg:locale message="Delete"/></button>
        </c:if>
        <c:if test="${sessionScope.User.getUserRole() eq 'CLIENT'}">
            <button type="submit" class="btn btn-primary" formaction="/addCarOrder" formmethod="post" ><ctg:locale message="OrderThisCar"/></button>
        </c:if>
    </form>

    <script src="libs/js/entityJQuery.js" type="text/javascript"></script>

    <a href="/cars"><ctg:locale message="CarList"/></a><br>

    <script type="text/javascript" src="libs/jquery-3.3.1/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="libs/propper-1.11.0/popper.min.js"></script>
    <script type="text/javascript" src="libs/bootstrap-4.1.3/js/bootstrap.min.js"></script>
</div>
</body>
</html>