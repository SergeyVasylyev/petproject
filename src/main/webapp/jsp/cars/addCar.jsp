<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="libs/bootstrap-4.1.3/css/bootstrap.min.css">
    <title><ctg:locale message="AddCar"/></title>
</head>
<body>
<jsp:include page="/jsp/header.jsp"/>
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-md-auto">
            <h4><ctg:locale message="AddCar"/></h4>
        </div>
    </div>

    <c:set var="validationErrorForm" value='${requestScope["validationError"]}' />

    <form action="/cars" method="post">
        <div class="form-group row">
            <label for="number" class="col-sm-2 col-form-label"><ctg:locale message="Number"/></label>
            <div class="col-sm-2">
                <input name="number" type="text" class="form-control" id="number" required placeholder="Number">
            </div>
        </div>
        <div class="form-group row">
            <label for="type" class="col-sm-2 col-form-label"><ctg:locale message="Type"/></label>
            <div class="col-sm-2">
                <select name="type" class="custom-select">
                    <option selected><ctg:locale message="ChooseType"/></option>
                    <option value="Audi">Audi</option>
                    <option value="Toyota">Toyota</option>
                    <option value="Nissan">Nissan</option>
                    <option value="Other">Other</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="quality" class="col-sm-2 col-form-label"><ctg:locale message="Quality"/></label>
            <div class="col-sm-2">
                <select name="quality" class="custom-select">
                    <option selected><ctg:locale message="ChooseQuality"/></option>
                    <option value="A">A</option>
                    <option value="B">B</option>
                    <option value="C">C</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label"><ctg:locale message="Name"/></label>
            <div class="col-sm-2">
                <input name="name" type="text" class="form-control" id="name" required placeholder="Name">
            </div>
        </div>
        <div class="form-group row">
            <label for="rentPrice" class="col-sm-2 col-form-label"><ctg:locale message="RentPrice"/></label>
            <div class="col-sm-2">
                <input name="rentPrice" type="text" class="form-control" id="rentPrice" required placeholder="rent price">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-10 col-form-label text-danger">${validationErrorForm}</label>
        </div>
        <button type="submit" class="btn btn-primary"><ctg:locale message="CreateCar"/></button>
    </form>

    <a href="/cars"><ctg:locale message="CarList"/></a><br>

    <script type="text/javascript" src="libs/jquery-3.3.1/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="libs/propper-1.11.0/popper.min.js"></script>
    <script type="text/javascript" src="libs/bootstrap-4.1.3/js/bootstrap.min.js"></script>
</div>
</body>
</html>