<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="libs/bootstrap-4.1.3/css/bootstrap.min.css">
    <title><ctg:locale message="CarList"/></title>
</head>
<body>
<jsp:include page="/jsp/header.jsp"/>
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-md-auto">
            <h4><ctg:locale message="CarList"/></h4>
        </div>
    </div>

    <c:set var="carList" value='${requestScope["carList"]}' />

    <form action="/cars" method="get" class = "form-horizontal" role = "form">
        <div class = "form-group row">
            <label for = "type" class = "col-1 col-form-label"><ctg:locale message="Type"/></label>
            <div class="col-2">
                <select name="type" class="custom-select">
                    <option selected></option>
                    <option value="Audi">Audi</option>
                    <option value="Toyota">Toyota</option>
                    <option value="Nissan">Nissan</option>
                    <option value="Other">Other</option>
                </select>
            </div>
            <label for = "quality" class = "col-1 col-form-label"><ctg:locale message="Quality"/></label>
            <div class="col-2">
                <select name="quality" class="custom-select">
                    <option selected></option>
                    <option value="A">A</option>
                    <option value="B">B</option>
                    <option value="C">C</option>
                </select>
            </div>
            <label for="sortType" class="col-2 col-form-label">   <ctg:locale message="SortBy"/></label>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="sortTypeOption" id="inlineRadio1" value="sortByName">
                <label class="form-check-label" for="inlineRadio1"><ctg:locale message="Name"/></label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="sortTypeOption" id="inlineRadio2" value="sortByRentPrice">
                <label class="form-check-label" for="inlineRadio2"><ctg:locale message="RentPrice"/></label>
            </div>
            <button type="submit" class="btn btn-primary" name="button" value="Filter"><ctg:locale message="Filter"/></button>
        </div>
    </form>
    <label></label>

    <table class="table table-striped table-bordered table-sm" cellspacing="0">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col"><ctg:locale message="Number"/></th>
                <th scope="col"><ctg:locale message="Type"/></th>
                <th scope="col"><ctg:locale message="Quality"/></th>
                <th scope="col"><ctg:locale message="Name"/></th>
                <th scope="col"><ctg:locale message="Price"/></th>
                <th scope="col"><ctg:locale message="Available"/></th>
                <th scope="col"><ctg:locale message="View"/></th>
            </tr>
        </thead>
        <tbody>
        <c:forEach items="${carList}" var="car" varStatus="status">
            <tr>
                <td>${car.getId()}</td>
                <td>${car.getNumber()}</td>
                <td>${car.getType()}</td>
                <td>${car.getQuality()}</td>
                <td>${car.getName()}</td>
                <td>${car.getRentPrice()}</td>
                <td>${car.isAvailable()}</td>
                <td><a href="/cars/${car.getId()}"><ctg:locale message="View"/></a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

    <c:if test="${sessionScope.User.getUserRole() eq 'ADMIN'}">
        <a href="/addCar"><ctg:locale message="AddCar"/></a><br>
    </c:if>
    <c:choose>
        <c:when test="${sessionScope.User.getUserRole() eq 'ADMIN'}">
            <form class="form-inline my-2 my-lg-0">
                <a href="/adminProfile"><ctg:locale message="Menu"/></a><br>
            </form>
        </c:when>
        <c:when test="${sessionScope.User.getUserRole() eq 'MANAGER'}">
            <form class="form-inline my-2 my-lg-0">
                <a href="/managerProfile"><ctg:locale message="Menu"/></a><br>
            </form>
        </c:when>
        <c:when test="${sessionScope.User.getUserRole() eq 'CLIENT'}">
            <form class="form-inline my-2 my-lg-0">
                <a href="/userProfile"><ctg:locale message="Menu"/></a><br>
            </form>
        </c:when>
    </c:choose>

    <script type="text/javascript" src="libs/jquery-3.3.1/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="libs/propper-1.11.0/popper.min.js"></script>
    <script type="text/javascript" src="libs/bootstrap-4.1.3/js/bootstrap.min.js"></script>
</div>
</body>
</html>
