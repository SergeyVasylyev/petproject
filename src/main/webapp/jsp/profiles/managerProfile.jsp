<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="libs/bootstrap-4.1.3/css/bootstrap.min.css">
    <title><ctg:locale message="ManagerProfile"/></title>
</head>
<body>
<jsp:include page="/jsp/header.jsp"/>
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-md-auto">
            <h4><ctg:locale message="ManagerProfile"/></h4>
        </div>
    </div>

    <c:set var="orderList" value='${requestScope["orderDTOList"]}' />
    <c:set var="orderListPayed" value='${requestScope["orderDTOListPayed"]}' />

    <c:if test="${orderList!=null && orderList.size()!=0}">
        <p><ctg:locale message="NewOrders"/> <c:out value="${orderList.size()}" /></p>
        <p><ctg:locale message="DetailedList"/></p>
        <table class="table table-striped table-bordered table-sm" cellspacing="0">
            <thead>
                <tr>
                    <th>ID</th>
                    <th><ctg:locale message="Car"/></th>
                    <th><ctg:locale message="Status"/></th>
                    <th><ctg:locale message="User"/></th>
                    <th><ctg:locale message="Sum"/></th>
                    <th><ctg:locale message="View"/></th>
                </tr>
            </thead>
            <tbody>
            <c:forEach items="${orderList}" var="order" varStatus="status">
                <tr>
                    <td>${order.getId()}</td>
                    <td>${order.getCar().toString()}</td>
                    <td>${order.getStatus()}</td>
                    <td>${order.getUser()}</td>
                    <td>${order.getSum()}</td>
                    <td><a href="/orders/${order.getId()}"><ctg:locale message="View"/></a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>

    <c:if test="${orderListPayed!=null && orderListPayed.size()!=0}">
        <p><ctg:locale message="PayedOrders"/> <c:out value="${orderListPayed.size()}" /></p>
        <p><ctg:locale message="DetailedList"/></p>
        <table class="table table-striped table-bordered table-sm" cellspacing="0">
            <thead>
                <tr>
                    <th>ID</th>
                    <th><ctg:locale message="Car"/></th>
                    <th><ctg:locale message="Status"/></th>
                    <th><ctg:locale message="User"/></th>
                    <th><ctg:locale message="Sum"/></th>
                    <th><ctg:locale message="View"/></th>
                </tr>
            </thead>
            <tbody>
            <c:forEach items="${orderListPayed}" var="order" varStatus="status">
                <tr>
                    <td>${order.getId()}</td>
                    <td>${order.getCar().toString()}</td>
                    <td>${order.getStatus()}</td>
                    <td>${order.getUser()}</td>
                    <td>${order.getSum()}</td>
                    <td><a href="/orders/${order.getId()}"><ctg:locale message="View"/></a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>

    <a href="/cars"><ctg:locale message="CarList"/></a><br>
    <a href="/orders"><ctg:locale message="OrderList"/></a><br>
    <a href="/invoices"><ctg:locale message="InvoiceList"/></a><br>

    <script type="text/javascript" src="libs/jquery-3.3.1/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="libs/propper-1.11.0/popper.min.js"></script>
    <script type="text/javascript" src="libs/bootstrap-4.1.3/js/bootstrap.min.js"></script>
</div>
</body>
</html>