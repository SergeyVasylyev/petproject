<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="libs/bootstrap-4.1.3/css/bootstrap.min.css">
    <title><ctg:locale message="ClientProfile"/></title>
</head>
<body>
<jsp:include page="/jsp/header.jsp"/>
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-md-auto">
            <h4><ctg:locale message="ClientProfile"/></h4>
        </div>
    </div>

    <c:set var="invoiceList" value='${requestScope["invoiceList"]}' />
    <c:set var="invoiceListIssue" value='${requestScope["invoiceListIssue"]}' />

    <c:if test="${invoiceList!=null && invoiceList.size()!=0}">
        <p><ctg:locale message="InvoicesNew"/> <c:out value="${invoiceList.size()}" /></p>
        <p><ctg:locale message="DetailedList"/></p>
        <table class="table table-striped table-bordered table-sm" cellspacing="0">
            <thead>
            <tr>
                <th>ID</th>
                <th><ctg:locale message="Order"/></th>
                <th><ctg:locale message="Status"/></th>
                <th><ctg:locale message="Pay"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${invoiceList}" var="invoice" varStatus="status">
                <tr>
                    <td>${invoice.getId()}</td>
                    <td>${invoice.getOrder()}</td>
                    <td>${invoice.getOrder().getStatus()}</td>
                    <td><a href="/invoices/${invoice.getId()}"><ctg:locale message="Pay"/></a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>

    <c:if test="${invoiceListIssue!=null && invoiceListIssue.size() != 0}">
        <p><ctg:locale message="InvoicesIssue"/> <c:out value="${invoiceListIssue.size()}" /></p>
        <p><ctg:locale message="DetailedList"/></p>
        <table class="table table-striped table-bordered table-sm" cellspacing="0">
            <thead>
            <tr>
                <th>ID</th>
                <th><ctg:locale message="Order"/></th>
                <th><ctg:locale message="Status"/></th>
                <th><ctg:locale message="Pay"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${invoiceListIssue}" var="invoice" varStatus="status">
                <tr>
                    <td>${invoice.getId()}</td>
                    <td>${invoice.getOrder()}</td>
                    <td>${invoice.getOrder().getStatus()}</td>
                    <td><a href="/invoices/${invoice.getId()}"><ctg:locale message="Pay"/></a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>

    <a href="/cars"><ctg:locale message="CarList"/></a><br>
    <a href="/orders"><ctg:locale message="MyOrderList"/></a><br>
    <a href="/invoices"><ctg:locale message="MyInvoiceList"/></a><br>

    <script type="text/javascript" src="libs/jquery-3.3.1/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="libs/propper-1.11.0/popper.min.js"></script>
    <script type="text/javascript" src="libs/bootstrap-4.1.3/js/bootstrap.min.js"></script>
</div>
</body>
</html>