<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/libs/bootstrap-4.1.3/css/bootstrap.min.css">
    <title><ctg:locale message="ViewOrder"/></title>
</head>
<body>
<jsp:include page="/jsp/header.jsp"/>
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-md-auto">
            <h4><ctg:locale message="ViewOrder"/></h4>
        </div>
    </div>

    <c:set var="order" value='${requestScope["orderDTO"]}' />

    <form action="/viewOrder" method="get">
        <input name="id" type="hidden" class="form-control" id="id" value=${order.getId()} >
        <input name="carId" type="hidden" class="form-control" id="carId" value=${order.getCar().getId()}>
        <div class="form-group row">
            <label for="carView" class="col-sm-2 col-form-label"><ctg:locale message="Car"/></label>
            <div class="col-sm-2">
                <input name="carView" type="text" class="form-control" id="carView" readonly value=${order.getCar().toString()}>
            </div>
            <div class="form-check">
                <input name="driverIncluded" type="checkbox" class="form-check-input" id="driverIncluded" disabled
                    <c:if test="${order.isDriverIncluded()}"> checked </c:if>>
                <label class="form-check-label" for="driverIncluded"><ctg:locale message="DriverIncluded"/></label>
            </div>
        </div>
        <div class="form-group row">
            <label for="passport" class="col-sm-2 col-form-label"><ctg:locale message="Passport"/></label>
            <div class="col-sm-2">
                <input name="passport" type="text" class="form-control" id="passport" readonly value=${order.getPassport()}>
            </div>
        </div>
        <div class="form-group row">
            <label for="days" class="col-sm-2 col-form-label"><ctg:locale message="RentDays"/></label>
            <div class="col-sm-2">
                <input name="days" type="text" class="form-control" id="days" readonly value=${order.getDays()} >
            </div>
        </div>
        <div class="form-group row">
            <label for="sum" class="col-sm-2 col-form-label"><ctg:locale message="Sum"/></label>
            <div class="col-sm-2">
                <input name="sum" type="text" class="form-control" id="sum" readonly value=${order.getSum()}>
            </div>
        </div>
        <div class="form-group row">
            <label for="comment" class="col-sm-2 col-form-label"><ctg:locale message="Comment"/></label>
            <div class="col-sm-2">
                <input name="comment" type="text" class="form-control" id="comment"
                    <c:if test="${not (order.getStatus().toString() eq 'NEW' && sessionScope.User.getUserRole() eq 'MANAGER')}"> disabled </c:if>
                        value=${order.getComment()}>
            </div>
        </div>
        <input name="userId" type="hidden" class="form-control" id="userId" value=${order.getUser().getUserId()} >
        <div class="form-group row">
            <label for="userView" class="col-sm-2 col-form-label"><ctg:locale message="User"/></label>
            <div class="col-sm-2">
                <input name="userView" type="text" class="form-control" id="userView" readonly value=${order.getUser().toString()}>
            </div>
        </div>
        <input name="status" type="hidden" class="form-control" id="status" value=${order.getStatus().toString()}>

        <c:if test="${sessionScope.User.getUserRole() eq 'ADMIN'}">
            <button type="submit" class="btn btn-primary" formaction="/deleteOrder" formmethod="post" ><ctg:locale message="Delete"/></button>
        </c:if>
        <c:if test="${sessionScope.User.getUserRole() eq 'MANAGER' && order.getStatus().toString() eq 'NEW'}">
            <button type="submit" class="btn btn-primary" formaction="/rejectOrder" formmethod="post" ><ctg:locale message="Reject"/></button>
            <button type="submit" class="btn btn-primary" formaction="/createInvoice" formmethod="post" ><ctg:locale message="CreateInvoice"/></button>
        </c:if>
        <c:if test="${sessionScope.User.getUserRole() eq 'MANAGER' && order.getStatus().toString() eq 'PAYED'}">
            <button type="submit" class="btn btn-primary" formaction="/createInvoiceIssue" formmethod="post" ><ctg:locale message="CreateInvoiceIssue"/></button>
            <button type="submit" class="btn btn-primary" formaction="/closeOrder" formmethod="post" ><ctg:locale message="CloseOrder"/></button>
        </c:if>
    </form>

    <a href="/orders"><ctg:locale message="OrderList"/></a><br>
    <c:choose>
        <c:when test="${sessionScope.User.getUserRole() eq 'ADMIN'}">
            <a href="/adminProfile"><ctg:locale message="Menu"/></a><br>
        </c:when>
        <c:when test="${sessionScope.User.getUserRole() eq 'MANAGER'}">
            <a href="/managerProfile"><ctg:locale message="Menu"/></a><br>
        </c:when>
        <c:when test="${sessionScope.User.getUserRole() eq 'CLIENT'}">
            <a href="/userProfile"><ctg:locale message="Menu"/></a><br>
        </c:when>
    </c:choose>

    <script type="text/javascript" src="libs/jquery-3.3.1/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="libs/propper-1.11.0/popper.min.js"></script>
    <script type="text/javascript" src="libs/bootstrap-4.1.3/js/bootstrap.min.js"></script>
</div>
</body>
</html>