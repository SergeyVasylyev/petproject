<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="libs/bootstrap-4.1.3/css/bootstrap.min.css">
    <title><ctg:locale message="AddOrder"/></title>
</head>
<body>
<jsp:include page="/jsp/header.jsp"/>
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-md-auto">
            <h4><ctg:locale message="AddOrder"/></h4>
        </div>
    </div>

    <c:set var="car" value='${requestScope["carDTO"]}' />
    <c:set var="validationErrorForm" value='${requestScope["validationError"]}' />

    <form action="/orders" method="post">
        <input name="carId" type="hidden" class="form-control" id="carId" value=${car.getId()}>
        <div class="form-group row">
            <label for="carView" class="col-sm-2 col-form-label"><ctg:locale message="Car"/></label>
            <div class="col-sm-4">
                <input name="carView" type="text" class="form-control" id="carView" readonly value=${car}>
            </div>
            <div class="form-check mb-2 mr-sm-2">
                <input name="driverIncluded" type="checkbox" class="form-check-input" id="driverIncluded">
                <label class="form-check-label" for="driverIncluded"><ctg:locale message="DriverIncluded"/></label>
            </div>
        </div>
        <div class="form-group row">
            <label for="passport" class="col-sm-2 col-form-label"><ctg:locale message="Passport"/></label>
            <div class="col-sm-4">
                <input name="passport" type="text" class="form-control" id="passport" placeholder="Passport" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="rentPrice" class="col-sm-2 col-form-label"><ctg:locale message="RentPrice"/></label>
            <div class="col-sm-2">
                <input name="rentPrice" type="number" class="form-control" id="rentPrice" readonly value=${car.getRentPrice()}>
            </div>
        </div>
        <div class="form-group row">
            <label for="days" class="col-sm-2 col-form-label"><ctg:locale message="RentDays"/></label>
            <div class="col-sm-2">
                <input name="days" type="number" class="form-control" id="days" placeholder="Rent days" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="comment" class="col-sm-2 col-form-label"><ctg:locale message="Comment"/></label>
            <div class="col-sm-4">
                <input name="comment" type="text" class="form-control" id="comment" placeholder="Comment">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-10 col-form-label text-danger">${validationErrorForm}</label>
        </div>
        <button type="submit" class="btn btn-primary" formaction="/orders" formmethod="post" ><ctg:locale message="CreateOrder"/></button>
    </form>

    <a href="/cars"><ctg:locale message="CarList"/></a><br>
    <a href="/orders"><ctg:locale message="OrderList"/></a><br>

    <script type="text/javascript" src="libs/jquery-3.3.1/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="libs/propper-1.11.0/popper.min.js"></script>
    <script type="text/javascript" src="libs/bootstrap-4.1.3/js/bootstrap.min.js"></script>
</div>
</body>
</html>