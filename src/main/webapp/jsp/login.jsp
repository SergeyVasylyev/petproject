<%@ page contentType="text/html;charset=utf-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<head>
    <meta http-equiv='content-type' content='text/html; charset=utf-8' >
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="../libs/bootstrap-4.1.3/css/bootstrap.min.css">
    <title>Log in</title>
</head>

<body>
<jsp:include page="/jsp/header.jsp"/>
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-md-auto">
            <c:if test="${User==null}">
            </c:if>
        </div>
    </div>
<div class="row justify-content-center">
    <c:choose>
    <c:when test="${User == null}">
        <form class="col-lg-6" action="${pageContext.request.contextPath}/login" method="post">
            <div class="form-group">
                <label for="registrationEmail">Email</label>
                <input name="email" type="email" class="form-control" id="registrationEmail" >
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input name="password" type="password" class="form-control" id="exampleInputPassword1">
            </div>
            <br/>
            ${errorLoginPassMessage}
            <br/>
            <div class="row justify-content-center">
                <button type="submit" class="btn btn-primary" >Login</button>
            </div>
        </form>
    </c:when>
    <c:otherwise>
        <div class="row justify-content-md-center">
            <c:choose>
                <c:when test="${sessionScope.User.getUserStatus() eq 'BLOCKED'}">
                    <div class="col-md-auto">
                        <h1><ctg:locale message="userBlocked"/></h1>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="col-md-auto">
                        <h1><ctg:locale message="alreadySignIn"/></h1>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </c:otherwise>
    </c:choose>
</div>
</div>
</div>

<script type="text/javascript" src="../libs/jquery-3.3.1/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="../libs/propper-1.11.0/popper.min.js"></script>
<script type="text/javascript" src="../libs/bootstrap-4.1.3/js/bootstrap.min.js"></script>
</body>
</html>
