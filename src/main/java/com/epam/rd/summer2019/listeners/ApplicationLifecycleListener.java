package com.epam.rd.summer2019.listeners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


public class ApplicationLifecycleListener implements ServletContextListener {

    //private PropertiesManager propertiesManager = new PropertiesManager();
    //private ConnectionH2 connectionUtils = new ConnectionH2();
    private static final Logger logger = LoggerFactory.getLogger(ApplicationLifecycleListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        //Properties applicationProperties = propertiesManager.getApplicationProperties();
        //connectionUtils.prepareDb();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        //connectionUtils.stopDb();
    }
}
