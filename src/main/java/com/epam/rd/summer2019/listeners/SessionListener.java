package com.epam.rd.summer2019.listeners;

import com.epam.rd.summer2019.dto.UserViewDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

@WebListener
public class SessionListener implements HttpSessionAttributeListener {

    private static final Logger logger = LoggerFactory.getLogger(SessionListener.class);

    @Override
    public void attributeAdded(HttpSessionBindingEvent event) {
        if (event.getName().compareTo("User") == 0) {
            UserViewDTO user = (UserViewDTO) event.getValue();
            logger.info("User " + user.getUserName() + " is logged in");
        }
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent event) {
        if (event.getName().compareTo("User") == 0) {
            UserViewDTO user = (UserViewDTO) event.getValue();
            logger.info("User " + user.getUserName() + " is logged out");
        }
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent event) {

    }
}
