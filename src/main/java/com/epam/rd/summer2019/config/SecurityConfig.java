package com.epam.rd.summer2019.config;

import com.epam.rd.summer2019.domain.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SecurityConfig {

    private Map<User.Role, List<String>> protectedURLs;
    private List<String> protectedURLWithoutRole;

    public SecurityConfig() {
        List<String> listAdminsPages = new ArrayList<>();
        listAdminsPages.add("/jsp/manager/managerOrder.jsp");
        listAdminsPages.add("/jsp/manager/managerProfile.jsp");
        listAdminsPages.add("/jsp/user/userProfile.jsp");
        listAdminsPages.add("/userProfile");
        listAdminsPages.add("/managerProfile");

        List<String> listManagersPages = new ArrayList<>();
        listManagersPages.add("/users");
        listManagersPages.add("/viewUser");
        listManagersPages.add("/jsp/viewUser.jsp");
        listManagersPages.add("/jsp/users.jsp");
        listManagersPages.add("/jsp/addUser.jsp");
        listManagersPages.add("/jsp/admin/addCar.jsp");
        listManagersPages.add("/jsp/admin/adminProfile.jsp");
        listManagersPages.add("/jsp/user/userProfile.jsp");
        listManagersPages.add("/adminProfile");
        listManagersPages.add("/userProfile");

        List<String> listUserPages = new ArrayList<>();
        listUserPages.add("/users");
        listUserPages.add("/viewUser");
        listUserPages.add("/jsp/viewUser.jsp");
        listUserPages.add("/jsp/users.jsp");
        listUserPages.add("/jsp/addUser.jsp");
        listUserPages.add("/jsp/addInvoice.jsp");
        listUserPages.add("/jsp/admin/addCar.jsp");
        listUserPages.add("/jsp/admin/adminProfile.jsp");
        listUserPages.add("/adminProfile");
        listUserPages.add("/managerProfile");
        listUserPages.add("/jsp/manager/managerOrder.jsp");
        listUserPages.add("/jsp/manager/managerProfile.jsp");

        protectedURLWithoutRole = new ArrayList<>();
        protectedURLWithoutRole.add("admin");
        protectedURLWithoutRole.add("user");
        protectedURLWithoutRole.add("manager");
        protectedURLWithoutRole.add("car");
        protectedURLWithoutRole.add("order");
        protectedURLWithoutRole.add("invoice");

        protectedURLs = new HashMap<>();
        protectedURLs.put(User.Role.ADMIN, listAdminsPages);
        protectedURLs.put(User.Role.MANAGER, listManagersPages);
        protectedURLs.put(User.Role.CLIENT, listUserPages);
    }

    public Map<User.Role, List<String>> getProtectedURLs() {
        return this.protectedURLs;
    }

    public List<String> getProtectedURLsWithoutRole() {
        return this.protectedURLWithoutRole;
    }
}
