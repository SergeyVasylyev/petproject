package com.epam.rd.summer2019.dto;

import com.epam.rd.summer2019.domain.Car;

public class CarViewDTO {

    private long id;
    private String number;
    private String type;
    private Car.QualityClass quality;
    private String name;
    private double rentPrice;
    private boolean available;

    public CarViewDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setId(String idString) {
        long id = (idString != null) ? Long.parseLong(idString) : 0;
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Car.QualityClass getQuality() {
        return quality;
    }

    public void setQuality(Car.QualityClass quality) {
        this.quality = quality;
    }

    public void setQuality(String qualityString) {
        Car.QualityClass quality = (qualityString != null) ? Car.QualityClass.valueOf(qualityString) : Car.QualityClass.A;
        this.quality = quality;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRentPrice() {
        return rentPrice;
    }

    public void setRentPrice(double rentPrice) {
        this.rentPrice = rentPrice;
    }

    public void setRentPrice(String rentPriceString) {
        double rentPrice = (rentPriceString != null) ? Double.parseDouble(rentPriceString) : 0;
        this.rentPrice = rentPrice;
    }

    public boolean isAvailable() {
        return available;
    }

    public String isAvailableString() {
        if (available) {
            return "on";
        } else {
            return "off";
        }
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public void setAvailable(String availableString) {
        if (availableString == "on") {
            this.available = true;
        } else {
            this.available = false;
        }
    }

    @Override
    public String toString() {
        return type + " " + name + " (" + number + ")";
    }
}
