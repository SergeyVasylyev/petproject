package com.epam.rd.summer2019.dto;

import com.epam.rd.summer2019.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;

public class InvoiceViewDTO {

    private long id;
    private String code;
    private OrderViewDTO order;
    private double sum;
    private boolean statusIssue;
    private OrderService orderService;

    @Autowired
    public InvoiceViewDTO(OrderService orderService) {
        this.orderService = orderService;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setId(String idString) {
        long id = (idString != null) ? Long.parseLong(idString) : 0;
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public OrderViewDTO getOrder() {
        return order;
    }

    public void setOrder(OrderViewDTO order) {
        this.order = order;
    }

    public void setOrder(String orderIdString) {
        long orderId = (orderIdString != null) ? Long.parseLong(orderIdString) : 0;
        this.order = orderService.findOrder(orderId);
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public void setSum(String sumString) {
        double sum = (sumString != null) ? Double.parseDouble(sumString) : 0;
        this.sum = sum;
    }

    public boolean isStatusIssue() {
        return statusIssue;
    }

    public void setStatusIssue(boolean statusIssue) {
        this.statusIssue = statusIssue;
    }

    @Override
    public String toString() {
        return "InvoiceViewDTO{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", order=" + order +
                ", sum=" + sum +
                ", issue=" + statusIssue +
                '}';
    }
}
