package com.epam.rd.summer2019.dto;

import com.epam.rd.summer2019.domain.User;

public class UserViewDTO {

    private long userId;
    private String firstName;
    private String lastName;
    private String userName;
    private String password;
    private String email;
    private User.Role userRole;
    private User.UserStatus userStatus;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public void setUserId(String idString) {
        long id = (idString != null) ? Long.parseLong(idString) : 0;
        this.userId = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User.Role getUserRole() {
        return userRole;
    }

    public String getUserRoleString() {
        return userRole.toString();
    }

    public void setUserRole(User.Role userRole) {
        this.userRole = userRole;
    }

    public void setUserRole(String userRoleString) {
        User.Role userRole = (userRoleString != null) ? User.Role.valueOf(userRoleString) : User.Role.CLIENT;
        this.userRole = userRole;
    }

    public User.UserStatus getUserStatus() {
        return userStatus;
    }

    public String getUserStatusString() {
        return userStatus.toString();
    }

    public void setUserStatus(User.UserStatus userStatus) {
        this.userStatus = userStatus;
    }

    public void setUserStatus(String userStatusString) {
        User.UserStatus userStatus = (userStatusString != null) ? User.UserStatus.valueOf(userStatusString) : User.UserStatus.ACTIVE;
        this.userStatus = userStatus;
    }

    @Override
    public String toString() {
        return userName + " (" + email + ")";
    }

    public String toStringLong() {
        return "UserViewDTO{" +
                "userId=" + userId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", userRole=" + userRole +
                ", userStatus=" + userStatus +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserViewDTO user = (UserViewDTO) o;

        if (userId != user.userId) return false;
        if (firstName != null ? !firstName.equals(user.firstName) : user.firstName != null) return false;
        if (lastName != null ? !lastName.equals(user.lastName) : user.lastName != null) return false;
        if (userName != null ? !userName.equals(user.userName) : user.userName != null) return false;
        if (email != null ? !email.equals(user.email) : user.email != null) return false;
        if (userRole != user.userRole) return false;
        return userStatus == user.userStatus;
    }

    @Override
    public int hashCode() {
        long result = userId;
        result = 18 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 18 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 18 * result + (userName != null ? userName.hashCode() : 0);
        result = 18 * result + (email != null ? email.hashCode() : 0);
        result = 18 * result + (userRole != null ? userRole.hashCode() : 0);
        result = 18 * result + (userStatus != null ? userStatus.hashCode() : 0);
        return (int) result;
    }
}
