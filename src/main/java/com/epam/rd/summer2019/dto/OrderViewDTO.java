package com.epam.rd.summer2019.dto;

import com.epam.rd.summer2019.domain.Order;
import com.epam.rd.summer2019.services.CarService;
import com.epam.rd.summer2019.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import static java.util.Objects.isNull;

public class OrderViewDTO {

    private long id;
    private String code;
    private String passport;
    private CarViewDTO car;
    private boolean driverIncluded;
    private int days;
    private double sum;
    private Order.Status status;
    private String comment;
    private UserViewDTO user;

    private CarService carService;
    private UserService userService;

    @Autowired
    public OrderViewDTO(CarService carService, UserService userService) {
        this.carService = carService;
        this.userService = userService;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setId(String idString) {
        long id = (!isNull(idString)) ? Long.parseLong(idString) : 0;
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public CarViewDTO getCar() {
        return car;
    }

    public void setCar(CarViewDTO car) {
        this.car = car;
    }

    public void setCar(String carIdString) {
        long carId = (!isNull(carIdString)) ? Long.parseLong(carIdString) : 0;
        this.car = carService.findCar(carId);
    }

    public boolean isDriverIncluded() {
        return driverIncluded;
    }

    public void setDriverIncluded(boolean driverIncluded) {
        this.driverIncluded = driverIncluded;
    }

    public void setDriverIncluded(String driverIncludedString) {
        if (isNull(driverIncludedString)) driverIncludedString = "";
        if (driverIncludedString.equals("on")) {
            this.driverIncluded = true;
        } else {
            this.driverIncluded = false;
        }
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public void setDays(String daysString) {
        int days = (!isNull(daysString)) ? Integer.parseInt(daysString) : 0;
        this.days = days;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public void setSum(String sumString) {
        double sum = (!isNull(sumString)) ? Double.parseDouble(sumString) : 0;
        this.sum = sum;
    }

    public Order.Status getStatus() {
        return status;
    }

    public void setStatus(Order.Status status) {
        this.status = status;
    }

    public void setStatus(String statusString) {
        Order.Status status = (!isNull(statusString)) ? Order.Status.valueOf(statusString) : Order.Status.NEW;
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = (comment != null) ? comment : "";
    }

    public UserViewDTO getUser() {
        return user;
    }

    public void setUser(UserViewDTO user) {
        this.user = user;
    }

    public void setUser(String userIdString) {
        long userId = (!isNull(userIdString)) ? Long.parseLong(userIdString) : 0;
        this.user = userService.findUser(userId);
    }

    public String toStringLong() {
        return "OrderViewDTO{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", passport='" + passport + '\'' +
                ", car=" + car +
                ", driverIncluded=" + driverIncluded +
                ", days=" + days +
                ", sum=" + sum +
                ", status=" + status +
                ", comment='" + comment + '\'' +
                ", user=" + user +
                '}';
    }

    @Override
    public String toString() {
        return id + "; " + car.toString() + "; " + days + " / " + sum;
    }
}
