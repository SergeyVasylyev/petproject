package com.epam.rd.summer2019;

import com.epam.rd.summer2019.config.WebAppConfig;
import com.epam.rd.summer2019.dao.CarDao;
import com.epam.rd.summer2019.services.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

public class App {

    public static void main(String[] args) {

        ApplicationContext context = new AnnotationConfigApplicationContext(WebAppConfig.class);
        TestServiceSpring testServiceSpring = (TestServiceSpring) context.getBean("testServiceSpring");

        testServiceSpring.getCar();
    }
}


@Component
class TestServiceSpring {
    @Autowired
    private CarDao carDao;
    @Autowired
    private CarService carService;

    public void getCar() {

        //System.out.println(carDao.getCarsList());
        //System.out.println(carService.findCar(1L));
        System.out.println(carService.findCar(1L));
        System.out.println(carService.getAllCars());
        System.out.println(carDao.getCarsList());

    }
}
