package com.epam.rd.summer2019.domain;

public class Invoice {

    private long id;
    private String code;
    private Order order;
    private double sum;
    private boolean statusIssue;

    private Invoice() {
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", order='" + order + '\'' +
                ", sum=" + sum +
                ", status=" + order.getStatus() +
                ", issue=" + statusIssue +
                '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public boolean isStatusIssue() {
        return statusIssue;
    }

    public void setStatusIssue(boolean statusIssue) {
        this.statusIssue = statusIssue;
    }

    public Order.Status getStatus() {
        return order.getStatus();
    }

    public static class Builder {
        private long id;
        private String code;
        private Order order;
        private double sum;
        private boolean statusIssue;

        public Builder(long id, String code, Order order) {
            this.id = id;
            this.code = code;
            this.order = order;
        }

        public Builder(String code, Order order) {
            this.code = code;
            this.order = order;
        }

        public Builder id(long vId) {
            id = vId;
            return this;
        }

        public Builder code(String vCode) {
            code = vCode;
            return this;
        }

        public Builder car(Order vOrder) {
            order = vOrder;
            return this;
        }

        public Builder sum(double vSum) {
            sum = vSum;
            return this;
        }

        public Builder statusIssue(boolean vStatusIssue) {
            statusIssue = vStatusIssue;
            return this;
        }

        public Invoice build() {
            return new Invoice(this);
        }
    }

    private Invoice(Builder builder) {
        id = builder.id;
        code = builder.code;
        order = builder.order;
        sum = builder.sum;
        statusIssue = builder.statusIssue;
    }
}
