package com.epam.rd.summer2019.domain;

public class Car {

    private long id;
    private String number;
    private String type;
    private QualityClass quality;
    private String name;
    private double rentPrice;
    private boolean available;

    public Car() {
    }

    public enum QualityClass {
        A, B, C
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public QualityClass getQuality() {
        return quality;
    }

    public void setQuality(QualityClass quality) {
        this.quality = quality;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRentPrice() {
        return rentPrice;
    }

    public void setRentPrice(float rentPrice) {
        this.rentPrice = rentPrice;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", type=" + type +
                ", quality=" + quality +
                ", name='" + name + '\'' +
                ", rent price'" + rentPrice + '\'' +
                ", available'" + available + '\'' +
                '}';
    }

    public String toStringShort() {
        return type + " " + name + " (" + number + ")";
    }

    public static class Builder {
        private long id;
        private String number;
        private String type;
        private QualityClass quality;
        private String name;
        private double rentPrice;
        private boolean available;

        public Builder(long id, String number) {
            this.id = id;
            this.number = number;
        }

        public Builder(String number) {
            this.number = number;
        }

        public Builder id(long vId) {
            id = vId;
            return this;
        }

        public Builder name(String vName) {
            name = vName;
            return this;
        }

        public Builder number(String vNumber) {
            number = vNumber;
            return this;
        }

        public Builder type(String vType) {
            type = vType;
            return this;
        }

        public Builder quality(QualityClass vQuality) {
            quality = vQuality;
            return this;
        }

        public Builder rentPrice(double vRentPrice) {
            rentPrice = vRentPrice;
            return this;
        }

        public Builder available(boolean vAvailable) {
            available = vAvailable;
            return this;
        }

        public Car build() {
            return new Car(this);
        }
    }

    private Car(Builder builder) {
        id = builder.id;
        name = builder.name;
        number = builder.number;
        type = builder.type;
        quality = builder.quality;
        rentPrice = builder.rentPrice;
        available = builder.available;
    }
}
