package com.epam.rd.summer2019.domain;

public class Order {

    private long id;
    private String code;
    private String passport;
    private Car car;
    private boolean driverIncluded;
    private int days;
    private double sum;
    private Status status;
    private String comment;
    private User user;

    private Order() {
    }

    public enum Status {
        NEW, REJECTED, APPROVED, PAYED, ISSUE, CLOSED
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", passport='" + passport + '\'' +
                ", car=" + car +
                ", driverIncluded=" + driverIncluded +
                ", days=" + days +
                ", sum=" + sum +
                ", status=" + status +
                ", comment='" + comment + '\'' +
                '}';
    }

    public String toStringShort() {
        return code + "; " + car.toStringShort() + "; " + days + " / " + sum + "; " + status;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public boolean isDriverIncluded() {
        return driverIncluded;
    }

    public void setDriverIncluded(boolean driverIncluded) {
        this.driverIncluded = driverIncluded;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public static class Builder {
        private long id;
        private String code;
        private String passport;
        private Car car;
        private boolean driverIncluded;
        private int days;
        private double sum;
        private Status status;
        private String comment;
        private User user;

        public Builder(long id, String code, String passport, Car car) {
            this.id = id;
            this.code = code;
            this.passport = passport;
            this.car = car;
        }

        public Builder(String code, String passport, Car car) {
            this.code = code;
            this.passport = passport;
            this.car = car;
        }

        public Builder id(long vId) {
            id = vId;
            return this;
        }

        public Builder code(String vCode) {
            code = vCode;
            return this;
        }

        public Builder car(Car vCar) {
            car = vCar;
            return this;
        }

        public Builder passport(String vPassport) {
            passport = vPassport;
            return this;
        }

        public Builder driverIncluded(boolean vDriverIncluded) {
            driverIncluded = vDriverIncluded;
            return this;
        }

        public Builder days(int vDays) {
            days = vDays;
            return this;
        }

        public Builder sum(double vSum) {
            sum = vSum;
            return this;
        }

        public Builder status(Status vStatus) {
            status = vStatus;
            return this;
        }

        public Builder comment(String vComment) {
            comment = vComment;
            return this;
        }

        public Builder user(User vUser) {
            user = vUser;
            return this;
        }

        public Order build() {
            return new Order(this);
        }
    }

    private Order(Builder builder) {
        id = builder.id;
        code = builder.code;
        passport = builder.passport;
        car = builder.car;
        driverIncluded = builder.driverIncluded;
        days = builder.days;
        sum = builder.sum;
        status = builder.status;
        comment = builder.comment;
        user = builder.user;
    }
}
