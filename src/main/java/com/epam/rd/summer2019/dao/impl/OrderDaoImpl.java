package com.epam.rd.summer2019.dao.impl;

import com.epam.rd.summer2019.dao.ConnectionAll;
import com.epam.rd.summer2019.dao.OrderDao;
import com.epam.rd.summer2019.domain.Car;
import com.epam.rd.summer2019.domain.Order;
import com.epam.rd.summer2019.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.rd.summer2019.dao.utils.CommonSQL.*;

@Repository
public class OrderDaoImpl implements OrderDao {

    private ConnectionAll connectionUtils;
    private static final Logger logger = LoggerFactory.getLogger(OrderDaoImpl.class);

    @Autowired
    public OrderDaoImpl(ConnectionAll connectionUtils) {
        this.connectionUtils = connectionUtils;
        try (Connection conn = connectionUtils.getConnection();
             Statement statement = conn.createStatement()
        ) {
            statement.execute(ORDER_SQL_CONSTRUCTOR);

        } catch (IOException | SQLException e) {
            logger.error("Error with SQL Connection! " + e.getMessage());
        }
    }

    @Override
    public void saveOrder(Order order) {
        if (order == null) {
            logger.error("Create order. Order can't be null!");
            return;
        }
        setStatement(ORDER_SQL_INSERT, order);
    }

    @Override
    public Order findOrder(Long id) {
        return findOrderCommon(ORDER_SQL_FIND_ID, id);
    }

    @Override
    public Order findOrder(String code) {
        return findOrderCommon(ORDER_SQL_FIND_NUMBER, code);
    }

    @Override
    public void modifyOrder(Order order) {
        logger.info("Save order " + order.getStatus().toString() + " // " + order.toString());
        if (order == null) {
            logger.error("Update order. Order can't be null!");
            return;
        }
        setStatement(ORDER_SQL_UPDATE, order);
    }

    @Override
    public List<Order> getOrderList() {
        try (Connection conn = connectionUtils.getConnection();
             PreparedStatement statement = conn.prepareStatement(ORDER_SQL_GET_LIST)) {
            try (ResultSet resultSet = statement.executeQuery();) {
                List<Order> result = new ArrayList<>();
                while (resultSet.next()) {
                    result.add(getOrderRomRS(resultSet));
                }
                return result;
            }
        } catch (IOException | SQLException e) {
            logger.error("Error with SQL Connection! " + e.getMessage());
        }
        return null;
    }

    @Override
    public void deleteOrder(Order order) {
        setStatement(ORDER_SQL_DELETE, order);
    }

    private void setStatement(String sqlSt, Order order) {
        try (Connection conn = connectionUtils.getConnection();
             PreparedStatement statement = conn.prepareStatement(sqlSt)) {
            switch (sqlSt) {
                case ORDER_SQL_UPDATE:
                    statement.setString(1, order.getCode());
                    statement.setString(2, order.getPassport());
                    statement.setLong(3, order.getCar().getId());
                    statement.setBoolean(4, order.isDriverIncluded());
                    statement.setInt(5, order.getDays());
                    statement.setDouble(6, order.getSum());
                    statement.setString(7, String.valueOf(order.getStatus()));
                    statement.setString(8, order.getComment());
                    statement.setLong(9, order.getUser().getUserId());
                    statement.setLong(10, order.getId());
                    break;
                case ORDER_SQL_DELETE:
                    statement.setLong(1, order.getId());
                    break;
                case ORDER_SQL_INSERT:
                    statement.setString(1, order.getCode());
                    statement.setString(2, order.getPassport());
                    statement.setLong(3, order.getCar().getId());
                    statement.setBoolean(4, order.isDriverIncluded());
                    statement.setInt(5, order.getDays());
                    statement.setDouble(6, order.getSum());
                    statement.setString(7, String.valueOf(order.getStatus()));
                    statement.setString(8, order.getComment());
                    statement.setLong(9, order.getUser().getUserId());
                    break;
            }
            statement.execute();
        } catch (IOException | SQLException e) {
            logger.error("Error with SQL Connection! " + e.getMessage());
        }
    }

    private <T> Order findOrderCommon(String sqlSt, T param) {
        try (Connection conn = connectionUtils.getConnection();
             PreparedStatement statement = conn.prepareStatement(sqlSt)) {
            switch (sqlSt) {
                case ORDER_SQL_FIND_NUMBER:
                    statement.setString(1, (String) param);
                    break;
                case ORDER_SQL_FIND_ID:
                    statement.setLong(1, (Long) param);
                    break;
            }
            try (ResultSet resultSet = statement.executeQuery();) {
                if (resultSet.next()) {
                    return getOrderRomRS(resultSet);
                }
            }
        } catch (IOException | SQLException e) {
            logger.error("Error with SQL Connection! " + e.getMessage());
        }
        return null;
    }

    public Order getOrderRomRS(ResultSet resultSet) throws SQLException {
        CarDaoImpl carDao = new CarDaoImpl(connectionUtils);
        Car car = carDao.getCarRomRS(resultSet);

        UserDaoImpl userDao = new UserDaoImpl(connectionUtils);
        User user = userDao.getUserRomRS(resultSet);

        long currentId = resultSet.getLong("orderId");
        String code = resultSet.getString("orderCode");
        String passport = resultSet.getString("passport");
        boolean driver = resultSet.getBoolean("driverIncluded");
        int days = resultSet.getInt("days");
        double sum = resultSet.getDouble("orderSum");
        String status = resultSet.getString("orderStatus");
        String comment = resultSet.getString("comment");

        return new Order.Builder(currentId, code, passport, car)
                .driverIncluded(driver)
                .days(days)
                .sum(sum)
                .status(Order.Status.valueOf(status))
                .comment(comment)
                .user(user)
                .build();
    }
}
