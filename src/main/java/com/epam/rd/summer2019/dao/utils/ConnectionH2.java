package com.epam.rd.summer2019.dao.utils;

import com.epam.rd.summer2019.config.PropertiesManager;
import com.epam.rd.summer2019.dao.ConnectionAll;
import com.epam.rd.summer2019.exceptions.ApplicationException;
import org.h2.tools.RunScript;
import org.h2.tools.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

@Repository
public class ConnectionH2 implements ConnectionAll {

    private Server server;
    private static final String SCHEMA_SCRIPT = "dbh2/schema.sql";
    private static final String DATA_SCRIPT = "dbh2/data.sql";
    private static final Logger logger = LoggerFactory.getLogger(ConnectionMBD.class);

    public Connection getConnection() throws SQLException, IOException {
        return getConnectionH2();
    }

    public void prepareDb() {
        try {
            server = Server.createTcpServer("-tcpAllowOthers").start();
            Class.forName("org.h2.Driver");
            Connection connection = getConnectionH2();
            RunScript.execute(connection, PropertiesManager.loadScript(SCHEMA_SCRIPT));
            RunScript.execute(connection, PropertiesManager.loadScript(DATA_SCRIPT));
        } catch (Exception e) {
            throw new ApplicationException("Failed to start the DB", e);
        }
    }

    public void stopDb() {
        if (server != null) {
            server.stop();
        }
    }

    public Connection getConnectionH2() throws SQLException, IOException {
        PropertiesManager propertiesManager = new PropertiesManager();
        Properties props = propertiesManager.getApplicationProperties();
        try {
            String url = props.getProperty("db.url");
            String username = props.getProperty("db.username");
            String password = props.getProperty("db.password");
            return DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            throw new ApplicationException("Failed to open the DB connection", e);
        }
    }
}
