package com.epam.rd.summer2019.dao;

import com.epam.rd.summer2019.domain.Order;

import java.util.List;

public interface OrderDao {
    /*
     * save Order by link
     * */
    void saveOrder(Order order);

    /*
     * find Order with id
     * return Order
     * */
    Order findOrder(Long id);

    /*
     * find Order with id
     * return Order
     * */
    Order findOrder(String code);

    /*
     * edit Order by link. Set up order data
     * */
    void modifyOrder(Order order);

    /*
     * return list of Orders
     * */
    List<Order> getOrderList();

    /*
     * delete Order by link with id
     * */
    void deleteOrder(Order order);
}
