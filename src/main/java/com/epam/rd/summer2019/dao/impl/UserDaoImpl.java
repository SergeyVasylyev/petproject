package com.epam.rd.summer2019.dao.impl;

import com.epam.rd.summer2019.dao.ConnectionAll;
import com.epam.rd.summer2019.dao.UserDao;
import com.epam.rd.summer2019.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.rd.summer2019.dao.utils.CommonSQL.*;

@Repository
public class UserDaoImpl implements UserDao {

    private ConnectionAll connectionUtils;
    private static final Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);

    @Autowired
    public UserDaoImpl(ConnectionAll connectionUtils) {
        this.connectionUtils = connectionUtils;
        try (Connection conn = connectionUtils.getConnection();
             Statement statement = conn.createStatement()
        ) {
            statement.execute(USER_SQL_CONSTRUCTOR);

        } catch (IOException | SQLException e) {
            logger.error("Error with SQL Connection! " + e.getMessage());
        }
    }

    @Override
    public void saveUser(User user) {
        if (user == null) {
            logger.error("Create user. User can't be null!");
            return;
        }
        setStatement(USER_SQL_INSERT, user);
    }

    @Override
    public User findUser(Long id) {
        return findUserCommon(USER_SQL_FIND_ID, id);
    }

    @Override
    public User findUser(String email) {
        return findUserCommon(USER_SQL_FIND_NUMBER, email);
    }

    @Override
    public void modifyUser(User user) {
        if (user == null) {
            logger.error("Update user. User can't be null!");
            return;
        }
        setStatement(USER_SQL_UPDATE, user);
    }

    @Override
    public List<User> getUserList() {
        try (Connection conn = connectionUtils.getConnection();
             PreparedStatement statement = conn.prepareStatement(USER_SQL_GET_LIST)) {
            try (ResultSet resultSet = statement.executeQuery();) {
                List<User> result = new ArrayList<>();
                while (resultSet.next()) {
                    result.add(getUserRomRS(resultSet));
                }
                return result;
            }
        } catch (IOException | SQLException e) {
            logger.error("Error with SQL Connection! " + e.getMessage());
        }
        return null;
    }

    @Override
    public void deleteUser(User user) {
        setStatement(USER_SQL_DELETE, user);
    }

    private void setStatement(String sqlSt, User user) {
        try (Connection conn = connectionUtils.getConnection();
             PreparedStatement statement = conn.prepareStatement(sqlSt)) {
            switch (sqlSt) {
                case USER_SQL_UPDATE:
                    statement.setString(1, user.getFirstName());
                    statement.setString(2, user.getLastName());
                    statement.setString(3, user.getUserName());
                    statement.setString(4, user.getPassword());
                    statement.setString(5, user.getEmail());
                    statement.setString(6, String.valueOf(user.getUserRole()));
                    statement.setString(7, String.valueOf(user.getUserStatus()));
                    statement.setLong(8, user.getUserId());
                    break;
                case USER_SQL_DELETE:
                    statement.setLong(1, user.getUserId());
                    break;
                case USER_SQL_INSERT:
                    statement.setString(1, user.getFirstName());
                    statement.setString(2, user.getLastName());
                    statement.setString(3, user.getUserName());
                    statement.setString(4, user.getPassword());
                    statement.setString(5, user.getEmail());
                    statement.setString(6, String.valueOf(user.getUserRole()));
                    statement.setString(7, String.valueOf(user.getUserStatus()));
                    break;
            }
            statement.execute();
        } catch (IOException | SQLException e) {
            logger.error("Error with SQL Connection! " + e.getMessage());
        }
    }

    private <T> User findUserCommon(String sqlSt, T param) {
        try (Connection conn = connectionUtils.getConnection();
             PreparedStatement statement = conn.prepareStatement(sqlSt)) {
            switch (sqlSt) {
                case USER_SQL_FIND_NUMBER:
                    statement.setString(1, (String) param);
                    break;
                case USER_SQL_FIND_ID:
                    statement.setLong(1, (Long) param);
                    break;
            }
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return getUserRomRS(resultSet);
                }
            }
        } catch (IOException | SQLException e) {
            logger.error("Error with SQL Connection! " + e.getMessage());
        }
        return null;
    }

    public User getUserRomRS(ResultSet resultSet) throws SQLException {

        long currentId = resultSet.getLong("userId");
        String firstName = resultSet.getString("firstName");
        String lastName = resultSet.getString("lastName");
        String userName = resultSet.getString("userName");
        String password = resultSet.getString("password");
        String email = resultSet.getString("email");
        String userRole = resultSet.getString("userRole");
        String userStatus = resultSet.getString("userStatus");

        return new User(currentId
                , firstName
                , lastName
                , userName
                , password
                , email
                , User.Role.valueOf(userRole)
                , User.UserStatus.valueOf(userStatus)
        );
    }
}
