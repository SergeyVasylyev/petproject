package com.epam.rd.summer2019.dao.utils;

import com.epam.rd.summer2019.dao.ConnectionAll;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

@Repository
@Primary
public class ConnectionMBD implements ConnectionAll {

    private static final Logger logger = LoggerFactory.getLogger(ConnectionMBD.class);

    public Connection getConnection() throws SQLException, IOException {
        return getConnectionMariaDB();
    }

    public Connection getConnectionMariaDB() throws SQLException, IOException {
        Properties props = new Properties();
        try (InputStream in =
                     ConnectionMBD.class.getClassLoader().getResourceAsStream("db.properties")) {
            props.load(in);
        }
        String drivers = props.getProperty("db.driver");
        if (drivers != null) {
            System.setProperty("db.driver", drivers);
        }
        String url = props.getProperty("db.url");
        String username = props.getProperty("db.user");
        String password = props.getProperty("db.password");

        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
        }
        return DriverManager.getConnection(url, username, password);
    }
}
