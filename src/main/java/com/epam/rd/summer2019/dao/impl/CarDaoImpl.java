package com.epam.rd.summer2019.dao.impl;

import com.epam.rd.summer2019.dao.CarDao;
import com.epam.rd.summer2019.dao.ConnectionAll;
import com.epam.rd.summer2019.domain.Car;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.rd.summer2019.dao.utils.CommonSQL.*;

@Repository
public class CarDaoImpl implements CarDao {

    private ConnectionAll connectionUtils;
    private static final Logger logger = LoggerFactory.getLogger(CarDaoImpl.class);

    @Autowired
    public CarDaoImpl(ConnectionAll connectionUtils) {
        this.connectionUtils = connectionUtils;
        try (Connection conn = connectionUtils.getConnection();
             Statement statement = conn.createStatement()
        ) {
            statement.execute(CAR_SQL_CONSTRUCTOR);

        } catch (IOException | SQLException e) {
            logger.error("Error with SQL Connection (CarDaoImpl)! " + e.getMessage());
        }
    }

    @Override
    public void saveCar(Car car) {
        if (car == null) {
            logger.error("Create car. Car can't be null!");
            return;
        }
        setStatement(CAR_SQL_INSERT, car);
    }

    @Override
    public Car findCar(Long id) {
        return findCarCommon(CAR_SQL_FIND_ID, id);
    }

    @Override
    public Car findCar(String number) {
        return findCarCommon(CAR_SQL_FIND_NUMBER, number);
    }

    @Override
    public void modifyCar(Car car) {
        setStatement(CAR_SQL_UPDATE, car);
    }

    @Override
    public List<Car> getCarsList() {
        try (Connection conn = connectionUtils.getConnection();
             PreparedStatement statement = conn.prepareStatement(CAR_SQL_GET_LIST)) {
            try (ResultSet resultSet = statement.executeQuery();) {
                List<Car> result = new ArrayList<>();
                while (resultSet.next()) {
                    result.add(getCarRomRS(resultSet));
                }
                return result;
            }
        } catch (IOException | SQLException e) {
            logger.error("Error with SQL Connection (getCarsList)! " + e.getMessage());
        }
        return null;
    }

    @Override
    public void deleteCar(Car car) {
        setStatement(CAR_SQL_DELETE, car);
    }

    private void setStatement(String sqlSt, Car car) {
        try (Connection conn = connectionUtils.getConnection();
             PreparedStatement statement = conn.prepareStatement(sqlSt)) {
            switch (sqlSt) {
                case CAR_SQL_UPDATE:
                    statement.setString(1, car.getNumber());
                    statement.setString(2, car.getType());
                    statement.setString(3, String.valueOf(car.getQuality()));
                    statement.setString(4, car.getName());
                    statement.setDouble(5, car.getRentPrice());
                    statement.setBoolean(6, car.isAvailable());
                    statement.setLong(7, car.getId());
                    break;
                case CAR_SQL_DELETE:
                    statement.setLong(1, car.getId());
                    break;
                case CAR_SQL_INSERT:
                    statement.setString(1, car.getNumber());
                    statement.setString(2, car.getType());
                    statement.setString(3, String.valueOf(car.getQuality()));
                    statement.setString(4, car.getName());
                    statement.setDouble(5, car.getRentPrice());
                    statement.setBoolean(6, car.isAvailable());
                    break;
            }
            statement.execute();
        } catch (IOException | SQLException e) {
            logger.error("Error with SQL Connection (setStatement)! " + e.getMessage());
        }
    }

    private <T> Car findCarCommon(String sqlSt, T param) {
        try (Connection conn = connectionUtils.getConnection();
             PreparedStatement statement = conn.prepareStatement(sqlSt)) {
            switch (sqlSt) {
                case CAR_SQL_FIND_NUMBER:
                    statement.setString(1, (String) param);
                    break;
                case CAR_SQL_FIND_ID:
                    statement.setLong(1, (Long) param);
                    break;
            }
            try (ResultSet resultSet = statement.executeQuery();) {
                if (resultSet.next()) {
                    return getCarRomRS(resultSet);
                }
            }
        } catch (IOException | SQLException e) {
            logger.error("Error with SQL Connection (findCarCommon)! " + e.getMessage());
        }
        return null;
    }

    public Car getCarRomRS(ResultSet resultSet) throws SQLException {
        long currentId = resultSet.getLong("carId");
        String number = resultSet.getString("number");
        String type = resultSet.getString("type");
        String quality = resultSet.getString("quality");
        String name = resultSet.getString("name");
        double rentPrice = resultSet.getDouble("rentPrice");
        Boolean available = resultSet.getBoolean("available");

        return new Car.Builder(currentId, number)
                .type(type)
                .quality(Car.QualityClass.valueOf(quality))
                .name(name)
                .rentPrice(rentPrice)
                .available(available)
                .build();
    }
}
