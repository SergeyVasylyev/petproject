package com.epam.rd.summer2019.dao;

import com.epam.rd.summer2019.domain.Invoice;

import java.util.List;

public interface InvoiceDao {
    /*
     * save Invoice by link
     * */
    void saveInvoice(Invoice invoice);

    /*
     * find Invoice with id
     * return Invoice
     * */
    Invoice findInvoice(Long id);

    /*
     * find Invoice with id
     * return Invoice
     * */
    Invoice findInvoice(String code);

    /*
     * edit Invoice by link. Set up Invoice data
     * */
    void modifyInvoice(Invoice invoice);

    /*
     * return list of Invoices
     * */
    List<Invoice> getInvoiceList();

    /*
     * delete Invoice by link with id
     * */
    void deleteInvoice(Invoice invoice);
}
