package com.epam.rd.summer2019.dao;

import com.epam.rd.summer2019.domain.User;

import java.util.List;

public interface UserDao {

    void saveUser(User user);

    User findUser(Long id);

    User findUser(String email);

    void modifyUser(User user);

    List<User> getUserList();

    void deleteUser(User user);
}
