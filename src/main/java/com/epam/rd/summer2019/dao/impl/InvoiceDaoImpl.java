package com.epam.rd.summer2019.dao.impl;

import com.epam.rd.summer2019.dao.ConnectionAll;
import com.epam.rd.summer2019.dao.InvoiceDao;
import com.epam.rd.summer2019.domain.Invoice;
import com.epam.rd.summer2019.domain.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.rd.summer2019.dao.utils.CommonSQL.*;

@Repository
public class InvoiceDaoImpl implements InvoiceDao {

    private ConnectionAll connectionUtils;
    private static final Logger logger = LoggerFactory.getLogger(InvoiceDaoImpl.class);

    @Autowired
    public InvoiceDaoImpl(ConnectionAll connectionUtils) {
        this.connectionUtils = connectionUtils;
        try (Connection conn = connectionUtils.getConnection();
             Statement statement = conn.createStatement()
        ) {
            statement.execute(INVOICE_SQL_CONSTRUCTOR);

        } catch (IOException | SQLException e) {
            logger.error("Error with SQL Connection! " + e.getMessage());
        }
    }

    @Override
    public void saveInvoice(Invoice invoice) {
        if (invoice == null) {
            logger.error("Create invoice. Invoice can't be null!");
            return;
        }
        setStatement(INVOICE_SQL_INSERT, invoice);
    }

    @Override
    public Invoice findInvoice(Long id) {
        return findOrderCommon(INVOICE_SQL_FIND_ID, id);
    }

    @Override
    public Invoice findInvoice(String code) {
        return findOrderCommon(INVOICE_SQL_FIND_NUMBER, code);
    }

    @Override
    public void modifyInvoice(Invoice invoice) {
        if (invoice == null) {
            logger.error("Update invoice. Invoice can't be null!");
            return;
        }
        setStatement(INVOICE_SQL_UPDATE, invoice);
    }

    @Override
    public List<Invoice> getInvoiceList() {
        try (Connection conn = connectionUtils.getConnection();
             PreparedStatement statement = conn.prepareStatement(INVOICE_SQL_GET_LIST)) {
            try (ResultSet resultSet = statement.executeQuery();) {
                List<Invoice> result = new ArrayList<>();
                while (resultSet.next()) {
                    result.add(getInvoiceRomRS(resultSet));
                }
                return result;
            }
        } catch (IOException | SQLException e) {
            logger.error("Error with SQL Connection! " + e.getMessage());
        }
        return null;
    }

    @Override
    public void deleteInvoice(Invoice invoice) {
        setStatement(INVOICE_SQL_DELETE, invoice);
    }

    private void setStatement(String sqlSt, Invoice invoice) {
        try (Connection conn = connectionUtils.getConnection();
             PreparedStatement statement = conn.prepareStatement(sqlSt)) {
            switch (sqlSt) {
                case INVOICE_SQL_UPDATE:
                    statement.setString(1, invoice.getCode());
                    statement.setLong(2, invoice.getOrder().getId());
                    statement.setDouble(3, invoice.getSum());
                    statement.setInt(4, (invoice.isStatusIssue()) ? 1 : 0);
                    statement.setLong(5, invoice.getId());
                    break;
                case INVOICE_SQL_DELETE:
                    statement.setLong(1, invoice.getId());
                    break;
                case INVOICE_SQL_INSERT:
                    statement.setString(1, invoice.getCode());
                    statement.setLong(2, invoice.getOrder().getId());
                    statement.setDouble(3, invoice.getSum());
                    statement.setInt(4, (invoice.isStatusIssue()) ? 1 : 0);
                    break;
            }
            statement.execute();
        } catch (IOException | SQLException e) {
            logger.error("Error with SQL Connection! " + e.getMessage());
        }
    }

    private <T> Invoice findOrderCommon(String sqlSt, T param) {
        try (Connection conn = connectionUtils.getConnection();
             PreparedStatement statement = conn.prepareStatement(sqlSt)) {
            switch (sqlSt) {
                case INVOICE_SQL_FIND_NUMBER:
                    statement.setString(1, (String) param);
                    break;
                case INVOICE_SQL_FIND_ID:
                    statement.setLong(1, (Long) param);
                    break;
            }
            try (ResultSet resultSet = statement.executeQuery();) {
                if (resultSet.next()) {
                    return getInvoiceRomRS(resultSet);
                }
            }
        } catch (IOException | SQLException e) {
            logger.error("Error with SQL Connection! " + e.getMessage());
        }
        return null;
    }

    private Invoice getInvoiceRomRS(ResultSet resultSet) throws SQLException {
        OrderDaoImpl orderDao = new OrderDaoImpl(connectionUtils);
        Order order = orderDao.getOrderRomRS(resultSet);

        long currentId = resultSet.getLong("invoiceId");
        String code = resultSet.getString("InvoiceCode");
        double sum = resultSet.getDouble("InvoiceSum");
        int status = resultSet.getInt("InvoiceStatus");

        return new Invoice.Builder(currentId, code, order)
                .sum(sum)
                .statusIssue((status == 1) ? true : false)
                .build();
    }
}
