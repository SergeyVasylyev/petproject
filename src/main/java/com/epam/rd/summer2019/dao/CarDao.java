package com.epam.rd.summer2019.dao;

import com.epam.rd.summer2019.domain.Car;

import java.util.List;

public interface CarDao {
    /*
     * save Car by link
     * */
    void saveCar(Car Car);

    /*
     * find Car with id
     * return Car
     * */
    Car findCar(Long id);

    /*
     * find Car by number
     * use this method for validation
     * */
    Car findCar(String number);

    /*
     * edit Car by link. Set up car data
     * */
    void modifyCar(Car car);

    /*
     * return list of Cars
     * */
    List<Car> getCarsList();

    /*
     * delete Car by link
     * */
    void deleteCar(Car car);
}
