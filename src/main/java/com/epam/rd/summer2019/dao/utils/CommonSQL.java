package com.epam.rd.summer2019.dao.utils;

public class CommonSQL {

    //SQL Queries
    //common fields
    public static final String ORDER_FIELDS = "carOrder.id as orderId, carOrder.code as orderCode, carOrder.passport, carOrder.carId, carOrder.driverIncluded, carOrder.days, carOrder.sum as orderSum, carOrder.status as orderStatus, carOrder.comment, carOrder.userId ";
    public static final String CAR_FIELDS = "car.number, car.type, car.quality, car.name, car.rentPrice, car.available ";
    public static final String INVOICE_FIELDS = "invoice.id as invoiceId, invoice.code as invoiceCode, invoice.sum as invoiceSum, invoice.status as invoiceStatus ";
    public static final String USER_FIELDS = " firstName, lastName, userName, password, email, userRole, userStatus ";

    //car
    public static final String CAR_SQL_CONSTRUCTOR = "CREATE TABLE IF NOT EXISTS car (ID INT AUTO_INCREMENT, NUMBER VARCHAR(50), TYPE VARCHAR(50), QUALITY VARCHAR(10), NAME VARCHAR(50), rentPrice DOUBLE, available INT, PRIMARY KEY(id))";
    public static final String CAR_SQL_INSERT = "insert into car (number, type, quality, name, rentPrice, available) values (?, ?, ?, ?, ?, ?)";
    public static final String CAR_SQL_FIND_ID = "select id as carId, " + CAR_FIELDS + " from car where id = ?";
    public static final String CAR_SQL_FIND_NUMBER = "select id as carId, " + CAR_FIELDS + " from car where number = ?";
    public static final String CAR_SQL_UPDATE = "update car set number = ?, type = ?, quality = ?, name = ?, rentPrice = ?, available = ? where id = ?";
    public static final String CAR_SQL_GET_LIST = "select id as carId, " + CAR_FIELDS + " from car order by id";
    public static final String CAR_SQL_DELETE = "delete from car where id = ?";

    //order
    public static final String ORDER_SQL_CONSTRUCTOR = "CREATE TABLE IF NOT EXISTS carOrder (ID INT AUTO_INCREMENT, code VARCHAR(50), passport VARCHAR(50), carId INT, driverIncluded INT, days INT, sum DOUBLE, status VARCHAR(100), comment VARCHAR(100), userId INT, PRIMARY KEY(id))";
    public static final String ORDER_SQL_INSERT = "insert into carOrder (code, passport, carId, driverIncluded, days, sum, status, comment, userId) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
    public static final String ORDER_SQL_FIND_ID = "select " +
            " " + ORDER_FIELDS +
            "," + CAR_FIELDS +
            "," + USER_FIELDS +
            " from carOrder join car on car.id = carOrder.carId" +
            " join user on user.id = carOrder.userId" +
            " where carOrder.id = ?";
    public static final String ORDER_SQL_FIND_NUMBER = "select " +
            " " + ORDER_FIELDS +
            "," + CAR_FIELDS +
            "," + USER_FIELDS +
            " from carOrder join car on car.id = carOrder.carId" +
            " join user on user.id = carOrder.userId" +
            " where carOrder.code = ?";
    public static final String ORDER_SQL_UPDATE = "update carOrder set code = ?, passport = ?, carId = ?, driverIncluded = ?, days = ?, sum = ?, status = ?, comment = ?, userId = ? where id = ?";
    public static final String ORDER_SQL_GET_LIST = "select " +
            " " + ORDER_FIELDS +
            "," + CAR_FIELDS +
            "," + USER_FIELDS +
            " from carOrder join car on car.id = carOrder.carId" +
            " join user on user.id = carOrder.userId" +
            " order by carOrder.id";
    public static final String ORDER_SQL_DELETE = "delete from carOrder where id = ?";

    //invoice
    public static final String INVOICE_SQL_CONSTRUCTOR = "CREATE TABLE IF NOT EXISTS invoice (ID INT AUTO_INCREMENT, CODE VARCHAR(50), ORDERID INT, SUM DOUBLE, STATUS INT, PRIMARY KEY(id))";
    public static final String INVOICE_SQL_INSERT = "insert into invoice (code, orderId, sum, status) values (?, ?, ?, ?)";
    public static final String INVOICE_SQL_FIND_ID = "select " +
            " " + INVOICE_FIELDS +
            "," + ORDER_FIELDS +
            "," + CAR_FIELDS +
            "," + USER_FIELDS +
            " from invoice join carOrder on carOrder.id = invoice.orderId" +
            " join car on car.id = carOrder.carId" +
            " join user on user.id = carOrder.userId" +
            " where invoice.id = ?";
    public static final String INVOICE_SQL_FIND_NUMBER = "select " +
            " " + INVOICE_FIELDS +
            "," + ORDER_FIELDS +
            "," + CAR_FIELDS +
            "," + USER_FIELDS +
            " from invoice join carOrder on carOrder.id = invoice.orderId" +
            " join car on car.id = carOrder.carId" +
            " join user on user.id = carOrder.userId" +
            " where invoice.code = ?";
    public static final String INVOICE_SQL_UPDATE = "update invoice set code = ?, orderId = ?, sum = ?, status = ? where id = ?";
    public static final String INVOICE_SQL_GET_LIST = "select " +
            " " + INVOICE_FIELDS +
            "," + ORDER_FIELDS +
            "," + CAR_FIELDS +
            "," + USER_FIELDS +
            " from invoice join carOrder on carOrder.id = invoice.orderId" +
            " join car on car.id = carOrder.carId" +
            " join user on user.id = carOrder.userId" +
            " order by invoice.id";
    public static final String INVOICE_SQL_DELETE = "delete from invoice where id = ?";

    //user
    public static final String USER_SQL_CONSTRUCTOR = "CREATE TABLE IF NOT EXISTS user (ID INT AUTO_INCREMENT, firstName VARCHAR(50), lastName VARCHAR(50), userName VARCHAR(50), password VARCHAR(128), email VARCHAR(50), userRole VARCHAR(50), userStatus VARCHAR(50), PRIMARY KEY(id))";
    public static final String USER_SQL_INSERT = "insert into user (firstName, lastName, userName, password, email, userRole, userStatus ) values (?, ?, ?, ?, ?, ?, ?)";
    public static final String USER_SQL_FIND_ID = "select id as userId, " + USER_FIELDS + " from user where id = ?";
    public static final String USER_SQL_FIND_NUMBER = "select id as userId," + USER_FIELDS + " from user where email = ?";
    public static final String USER_SQL_UPDATE = "update user set firstName = ?, lastName = ?, userName = ?, password = ?, email = ?, userRole = ?, userStatus = ? where id = ?";
    public static final String USER_SQL_GET_LIST = "select id as userId, " + USER_FIELDS + " from user order by id";
    public static final String USER_SQL_DELETE = "delete from user where id = ?";
}
