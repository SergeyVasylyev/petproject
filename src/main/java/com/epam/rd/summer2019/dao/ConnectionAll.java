package com.epam.rd.summer2019.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public interface ConnectionAll {

    Connection getConnection() throws SQLException, IOException;

}
