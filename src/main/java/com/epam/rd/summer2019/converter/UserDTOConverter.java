package com.epam.rd.summer2019.converter;

import com.epam.rd.summer2019.domain.User;
import com.epam.rd.summer2019.dto.UserViewDTO;
import org.springframework.stereotype.Component;

@Component
public class UserDTOConverter {

    public UserViewDTO convertUserToDTO(User user) {
        UserViewDTO userViewDTO = new UserViewDTO();
        userViewDTO.setUserId(user.getUserId());
        userViewDTO.setFirstName(user.getFirstName());
        userViewDTO.setLastName(user.getLastName());
        userViewDTO.setUserName(user.getUserName());
        userViewDTO.setPassword(user.getPassword());
        userViewDTO.setEmail(user.getEmail());
        userViewDTO.setUserRole(user.getUserRole());
        userViewDTO.setUserStatus(user.getUserStatus());

        return userViewDTO;
    }

    public User convertDTOToUser(UserViewDTO userViewDTO) {
        User user = new User(userViewDTO.getUserId()
                , userViewDTO.getFirstName()
                , userViewDTO.getLastName()
                , userViewDTO.getUserName()
                , userViewDTO.getPassword()
                , userViewDTO.getEmail()
                , userViewDTO.getUserRole()
                , userViewDTO.getUserStatus()
        );

        return user;
    }
}
