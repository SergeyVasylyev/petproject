package com.epam.rd.summer2019.converter;

import com.epam.rd.summer2019.domain.Invoice;
import com.epam.rd.summer2019.dto.InvoiceViewDTO;
import com.epam.rd.summer2019.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InvoiceDTOConverter {

    private OrderDTOConverter orderDTOConverter;
    private OrderService orderService;

    @Autowired
    public InvoiceDTOConverter(OrderDTOConverter orderDTOConverter, OrderService orderService) {
        this.orderDTOConverter = orderDTOConverter;
        this.orderService = orderService;
    }

    public InvoiceViewDTO convertInvoiceToDTO(Invoice invoice) {
        InvoiceViewDTO invoiceViewDTO = new InvoiceViewDTO(orderService);
        invoiceViewDTO.setId(invoice.getId());
        invoiceViewDTO.setCode(invoice.getCode());
        invoiceViewDTO.setOrder(orderDTOConverter.convertOrderToDTO(invoice.getOrder()));
        invoiceViewDTO.setSum(invoice.getSum());
        invoiceViewDTO.setStatusIssue(invoice.isStatusIssue());
        return invoiceViewDTO;
    }

    public Invoice convertDTOToInvoice(InvoiceViewDTO invoiceViewDTO) {
        Invoice invoice = new Invoice.Builder(invoiceViewDTO.getId(), invoiceViewDTO.getCode(), orderDTOConverter.convertDTOToOrder(invoiceViewDTO.getOrder()))
                .sum(invoiceViewDTO.getSum())
                .statusIssue(invoiceViewDTO.isStatusIssue())
                .build();
        return invoice;
    }
}
