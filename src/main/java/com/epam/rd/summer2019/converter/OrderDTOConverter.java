package com.epam.rd.summer2019.converter;

import com.epam.rd.summer2019.domain.Order;
import com.epam.rd.summer2019.dto.OrderViewDTO;
import com.epam.rd.summer2019.services.CarService;
import com.epam.rd.summer2019.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderDTOConverter {

    private CarDTOConverter carDTOConverter;
    private UserDTOConverter userDTOConverter;
    private CarService carService;
    private UserService userService;

    @Autowired
    public OrderDTOConverter(CarDTOConverter carDTOConverter, UserDTOConverter userDTOConverter, CarService carService, UserService userService) {
        this.carDTOConverter = carDTOConverter;
        this.userDTOConverter = userDTOConverter;
        this.carService = carService;
        this.userService = userService;
    }

    public OrderViewDTO convertOrderToDTO(Order order) {
        OrderViewDTO orderViewDTO = new OrderViewDTO(carService, userService);
        orderViewDTO.setId(order.getId());
        orderViewDTO.setCode(order.getCode());
        orderViewDTO.setPassport(order.getPassport());
        orderViewDTO.setCar(carDTOConverter.convertCarToDTO(order.getCar()));
        orderViewDTO.setDriverIncluded(order.isDriverIncluded());
        orderViewDTO.setDays(order.getDays());
        orderViewDTO.setSum(order.getSum());
        orderViewDTO.setStatus(order.getStatus());
        orderViewDTO.setComment(order.getComment());
        orderViewDTO.setUser(userDTOConverter.convertUserToDTO(order.getUser()));
        return orderViewDTO;
    }

    public Order convertDTOToOrder(OrderViewDTO orderViewDTO) {
        Order order = new Order.Builder(orderViewDTO.getId(), orderViewDTO.getCode(), orderViewDTO.getPassport(), carDTOConverter.convertDTOToCar(orderViewDTO.getCar()))
                .driverIncluded(orderViewDTO.isDriverIncluded())
                .days(orderViewDTO.getDays())
                .sum(orderViewDTO.getSum())
                .status(orderViewDTO.getStatus())
                .comment(orderViewDTO.getComment())
                .user(userDTOConverter.convertDTOToUser(orderViewDTO.getUser()))
                .build();
        return order;
    }
}
