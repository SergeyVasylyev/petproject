package com.epam.rd.summer2019.converter;

import com.epam.rd.summer2019.domain.Car;
import com.epam.rd.summer2019.dto.CarViewDTO;
import org.springframework.stereotype.Component;

@Component
public class CarDTOConverter {

    public CarViewDTO convertCarToDTO(Car car) {
        CarViewDTO carViewDTO = new CarViewDTO();
        carViewDTO.setId(car.getId());
        carViewDTO.setNumber(car.getNumber());
        carViewDTO.setType(car.getType());
        carViewDTO.setQuality(car.getQuality());
        carViewDTO.setName(car.getName());
        carViewDTO.setRentPrice(car.getRentPrice());
        carViewDTO.setAvailable(car.isAvailable());
        return carViewDTO;
    }

    public Car convertDTOToCar(CarViewDTO carViewDTO) {
        Car car = new Car.Builder(carViewDTO.getId(), carViewDTO.getNumber())
                .type(carViewDTO.getType())
                .quality(carViewDTO.getQuality())
                .name(carViewDTO.getName())
                .rentPrice(carViewDTO.getRentPrice())
                .available(carViewDTO.isAvailable())
                .build();
        return car;
    }
}
