package com.epam.rd.summer2019.controllers.common;

import com.epam.rd.summer2019.domain.User;
import com.epam.rd.summer2019.dto.UserViewDTO;
import com.epam.rd.summer2019.exceptions.UserNotFoundException;
import com.epam.rd.summer2019.services.UserService;
import com.epam.rd.summer2019.util.PasswordEncryption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

@Controller
public class LogInController {

    private UserService userService;
    private static final Logger logger = LoggerFactory.getLogger(LogInController.class);

    @Autowired
    public LogInController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/login")
    public ModelAndView getPageLogin() {
        return new ModelAndView("/login");
    }

    @PostMapping(value = "/login")
    public void postPageLogin(HttpServletRequest req, HttpServletResponse resp) throws IOException, UserNotFoundException {

        Locale locale = (Locale) req.getSession().getAttribute("LOCALE");
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        UserViewDTO userViewDTO = userService.findUser(email);

        if (userViewDTO != null && PasswordEncryption.checkPassword(password, userViewDTO.getPassword())) {
            req.getSession().setAttribute("User", userViewDTO);
            req.setAttribute("Welcome", "Welcome to " + userViewDTO.getUserRole() + " page");
            if (userViewDTO.getUserStatus() == User.UserStatus.BLOCKED) {
                resp.sendRedirect("/jsp/login.jsp");
            } else {
                if (userViewDTO.getUserRole() == User.Role.ADMIN) {
                    resp.sendRedirect("/adminProfile");
                } else if (userViewDTO.getUserRole() == User.Role.CLIENT) {
                    resp.sendRedirect("/userProfile");
                } else {
                    resp.sendRedirect("/managerProfile");
                }
            }
        } else {
            {
                ResourceBundle messages = ResourceBundle.getBundle("i18n.messages", locale);
                String errorMsg = messages.getString("wrongLoginOrPassword");
                throw new UserNotFoundException(errorMsg);
            }
        }
    }
}
