package com.epam.rd.summer2019.controllers.entitylist;

import com.epam.rd.summer2019.domain.User;
import com.epam.rd.summer2019.dto.CarViewDTO;
import com.epam.rd.summer2019.dto.UserViewDTO;
import com.epam.rd.summer2019.exceptions.ApplicationException;
import com.epam.rd.summer2019.services.CarService;
import com.epam.rd.summer2019.validation.impl.CarValidationImpl;
import com.epam.rd.summer2019.validation.impl.ValidationFactoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

@Controller
public class CarController {

    private CarService carService;
    private static final Logger logger = LoggerFactory.getLogger(CarController.class);

    @Autowired
    public CarController(CarService carService) {
        this.carService = carService;
    }

    @GetMapping(value = "/cars")
    public ModelAndView getCarList(HttpServletRequest req) {

        List<CarViewDTO> carList;
        UserViewDTO user = (UserViewDTO) req.getSession().getAttribute("User");
        boolean available = (user != null && user.getUserRole() == User.Role.CLIENT);

        String button = req.getParameter("button");
        if (button == null) button = "";
        if (button.equals("Filter")) {
            String type = req.getParameter("type");
            String quality = req.getParameter("quality");
            String sortType = req.getParameter("sortTypeOption");
            if (sortType == null) sortType = "";
            carList = carService.getAllCarsFilterSort(type, quality, sortType, available);

        } else {
            carList = carService.getAllCars(available);
        }
        return new ModelAndView("/cars/cars", "carList", carList);
    }

    @GetMapping(value = "/cars/{carId}")
    public ModelAndView viewCarById(@PathVariable Long carId) {
        CarViewDTO carViewDTO = carService.findCar(carId);
        return new ModelAndView("/cars/viewCar", "carDTO", carViewDTO);
    }

    @PostMapping(value = "/updateCar")
    public String updateCar(@ModelAttribute("carViewDTO") CarViewDTO carViewDTO) {
        carService.modifyCar(carViewDTO);
        return "redirect: /cars";
    }

    @PostMapping(value = "/deleteCar")
    public String deleteCar(@ModelAttribute("carViewDTO") CarViewDTO carViewDTO) {
        carService.deleteCar(carViewDTO.getId());
        return "redirect: /cars";
    }

    @PostMapping(value = "/cars")
    public void addCar(@ModelAttribute("carViewDTO") CarViewDTO carViewDTO, HttpServletRequest req, HttpServletResponse resp) throws IOException {

        CarValidationImpl validation = ValidationFactoryImpl.getValidationFactory().getCarValidationImpl();
        String validationResult = validation.validate(req);

        if (validationResult.isEmpty()) {
            carService.createCar(carViewDTO);
            resp.sendRedirect("/cars");
        } else {
            Locale locale = (Locale) req.getSession().getAttribute("LOCALE");
            ResourceBundle messages = ResourceBundle.getBundle("i18n.messages", locale);
            throw new ApplicationException(messages.getString(validationResult), new ModelAndView("cars/addCar"));
        }
    }

    @PostMapping(value = "/addCarOrder")
    public ModelAndView addCarOrder(@ModelAttribute("carViewDTO") CarViewDTO carViewDTO) {
        return new ModelAndView("orders/addOrder", "carDTO", carViewDTO);
    }
}