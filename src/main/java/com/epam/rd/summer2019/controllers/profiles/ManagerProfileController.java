package com.epam.rd.summer2019.controllers.profiles;

import com.epam.rd.summer2019.domain.Order;
import com.epam.rd.summer2019.dto.OrderViewDTO;
import com.epam.rd.summer2019.services.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class ManagerProfileController {

    private OrderService orderService;
    private static final Logger logger = LoggerFactory.getLogger(ManagerProfileController.class);

    @Autowired
    public ManagerProfileController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping(value = "/managerProfile")
    public ModelAndView getManagerProfile() {
        List<OrderViewDTO> orderViewDTOList = orderService.getOrdersByStatus(Order.Status.NEW);
        List<OrderViewDTO> orderViewDTOListPayed = orderService.getOrdersByStatus(Order.Status.PAYED);

        ModelAndView modelAndView = new ModelAndView("/profiles/managerProfile");
        modelAndView.addObject("orderDTOList", orderViewDTOList);
        modelAndView.addObject("orderDTOListPayed", orderViewDTOListPayed);

        return modelAndView;
    }
}