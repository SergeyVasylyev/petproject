package com.epam.rd.summer2019.controllers.entitylist;

import com.epam.rd.summer2019.domain.Order;
import com.epam.rd.summer2019.domain.User;
import com.epam.rd.summer2019.dto.CarViewDTO;
import com.epam.rd.summer2019.dto.OrderViewDTO;
import com.epam.rd.summer2019.dto.UserViewDTO;
import com.epam.rd.summer2019.exceptions.ApplicationException;
import com.epam.rd.summer2019.services.CarService;
import com.epam.rd.summer2019.services.OrderService;
import com.epam.rd.summer2019.services.UserService;
import com.epam.rd.summer2019.services.calc.CalculationService;
import com.epam.rd.summer2019.services.statuses.CarStatusService;
import com.epam.rd.summer2019.services.statuses.OrderStatusService;
import com.epam.rd.summer2019.validation.impl.OrderValidationImpl;
import com.epam.rd.summer2019.validation.impl.ValidationFactoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

@Controller
public class OrderController {

    private OrderService orderService;
    private OrderStatusService statusService;
    private CarStatusService carStatusService;
    private CarService carService;
    private CalculationService calculationService;
    private UserService userService;
    private static final Logger logger = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    public OrderController(OrderService orderService, OrderStatusService statusService, UserService userService
            , CarStatusService carStatusService, CarService carService, CalculationService calculationService) {
        this.orderService = orderService;
        this.statusService = statusService;
        this.carStatusService = carStatusService;
        this.carService = carService;
        this.calculationService = calculationService;
        this.userService = userService;
    }

    @GetMapping(value = "/orders")
    public ModelAndView getOrderList(HttpServletRequest req) {

        List<OrderViewDTO> OrderList;
        HttpSession session = req.getSession();
        UserViewDTO user = (UserViewDTO) session.getAttribute("User");
        if (user.getUserRole() == User.Role.CLIENT) {
            OrderList = orderService.getOrdersByUser(user);
        } else {
            OrderList = orderService.getAllOrders();
        }
        return new ModelAndView("/orders/orders", "orderList", OrderList);
    }

    @GetMapping(value = "/orders/{orderId}")
    public ModelAndView viewOrder(@PathVariable Long orderId) {
        OrderViewDTO orderViewDTO = orderService.findOrder(orderId);
        return new ModelAndView("/orders/viewOrder", "orderDTO", orderViewDTO);
    }

    @PostMapping(value = "/orders")
    public void addOrder(@SessionAttribute("User") UserViewDTO userViewDTO
            , @RequestParam("carId") String carIdString, @RequestParam("passport") String passport
            , @RequestParam("days") String daysString
            , @RequestParam("comment") String comment
            , HttpServletRequest req, HttpServletResponse resp) throws IOException {

        OrderValidationImpl validation = ValidationFactoryImpl.getValidationFactory().getOrderValidationImpl();
        String validationResult = validation.validate(req);

        if (validationResult.isEmpty()) {

            OrderViewDTO orderViewDTO = new OrderViewDTO(carService, userService);
            orderViewDTO.setPassport(passport);
            orderViewDTO.setCar(carIdString);
            String driverIncludedString = req.getParameter("driverIncluded");
            orderViewDTO.setDriverIncluded(driverIncludedString);
            orderViewDTO.setDays(daysString);
            orderViewDTO.setSum(calculationService.getSum(req));
            orderViewDTO.setComment(comment);
            orderViewDTO.setStatus(Order.Status.NEW);
            if (userViewDTO.getUserRole() == User.Role.CLIENT) {
                orderViewDTO.setUser(userViewDTO);
            }

            carStatusService.changeCarStatus(orderViewDTO);
            orderService.createOrder(orderViewDTO);
            resp.sendRedirect("/orders");
        } else {
            long carId = Long.parseLong(req.getParameter("carId"));
            CarViewDTO carViewDTO = carService.findCar(carId);

            Locale locale = (Locale) req.getSession().getAttribute("LOCALE");
            ResourceBundle messages = ResourceBundle.getBundle("i18n.messages", locale);

            ModelAndView modelAndView = new ModelAndView("orders/addOrder");
            modelAndView.addObject("carDTO", carViewDTO);
            throw new ApplicationException(messages.getString(validationResult), modelAndView);
        }
    }

    @PostMapping(value = "/deleteOrder")
    public String deleteOrder(@ModelAttribute("orderViewDTO") OrderViewDTO orderViewDTO) {
        orderService.deleteOrder(orderViewDTO.getId());
        return "redirect: /orders";
    }

    @PostMapping(value = "/rejectOrder")
    public String rejectOrder(@RequestParam("id") Long orderId) {
        OrderViewDTO orderViewDTO = orderService.findOrder(orderId);
        statusService.changeOrderStatus(orderViewDTO, "Reject");
        return "redirect: /managerProfile";
    }

    @PostMapping(value = "/closeOrder")
    public String closeOrder(@RequestParam("id") Long orderId) {
        OrderViewDTO orderViewDTO = orderService.findOrder(orderId);
        statusService.changeOrderStatus(orderViewDTO, "");
        return "redirect: /managerProfile";
    }
}
