package com.epam.rd.summer2019.controllers.entitylist;

import com.epam.rd.summer2019.dto.UserViewDTO;
import com.epam.rd.summer2019.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {

    private UserService userService;
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/users")
    public ModelAndView getUserList() {
        return new ModelAndView("/users/users", "userList", userService.GetAllUsers());
    }

    @GetMapping(value = "/users/{userId}")
    public ModelAndView viewUser(@PathVariable Long userId) {
        UserViewDTO userViewDTO = userService.findUser(userId);
        return new ModelAndView("/users/viewUser", "userDTO", userViewDTO);
    }

    @PostMapping(value = "/users")
    public String addUser(@ModelAttribute("userViewDTO") UserViewDTO userViewDTO) {
        userService.createUser(userViewDTO);
        return "redirect: /users";
    }

    @PostMapping(value = "/updateUser")
    public String updateUser(@ModelAttribute("userViewDTO") UserViewDTO userViewDTO) {
        userService.modifyUser(userViewDTO);
        return "redirect: /users";
    }

    @PostMapping(value = "/deleteUser")
    public String deleteUser(@ModelAttribute("userViewDTO") UserViewDTO userViewDTO) {
        userService.deleteUser(userViewDTO.getUserId());
        return "redirect: /users";
    }
}
