package com.epam.rd.summer2019.controllers.common;

import com.epam.rd.summer2019.domain.User;
import com.epam.rd.summer2019.dto.UserViewDTO;
import com.epam.rd.summer2019.services.UserService;
import com.epam.rd.summer2019.util.PasswordEncryption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

@Controller
public class RegistrationController {

    private UserService userService;
    private static final Logger logger = LoggerFactory.getLogger(RegistrationController.class);

    @Autowired
    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/registration")
    public ModelAndView getRegistration() {
        return new ModelAndView("/login");
    }

    @PostMapping(value = "/registration")
    public void postRegistration(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");
        UserViewDTO tempUser = userService.findUser(email);
        if (tempUser == null) {
            UserViewDTO userViewDTO = getUserParametersFromRequest(req);
            userService.createUser(userViewDTO);
            req.getRequestDispatcher("/jsp/login.jsp").forward(req, resp);
            logger.info("New account created for user " + userViewDTO.getUserName());
        } else {
            Locale locale = (Locale) req.getSession().getAttribute("LOCALE");
            ResourceBundle messages = ResourceBundle.getBundle("i18n.messages", locale);
            req.setAttribute("IncorrectData", messages.getString("wrongData"));
            req.getRequestDispatcher("/jsp/registration.jsp").forward(req, resp);
        }
    }

    private UserViewDTO getUserParametersFromRequest(HttpServletRequest req) {
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String userName = req.getParameter("userName");
        String password = req.getParameter("password");
        String email = req.getParameter("email");

        UserViewDTO userViewDTO = new UserViewDTO();
        userViewDTO.setFirstName(firstName);
        userViewDTO.setLastName(lastName);
        userViewDTO.setEmail(email);
        userViewDTO.setPassword(password);
        userViewDTO.setUserName(userName);
        userViewDTO.setUserStatus(User.UserStatus.ACTIVE.toString());
        userViewDTO.setUserRole(User.Role.CLIENT.toString());
        encryptUserPassword(userViewDTO);

        return userViewDTO;
    }

    private void encryptUserPassword(UserViewDTO user) {
        String encryptPassword = PasswordEncryption.encryptPassword(user.getPassword());
        user.setPassword(encryptPassword);
    }

}
