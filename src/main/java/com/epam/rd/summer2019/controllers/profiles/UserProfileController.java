package com.epam.rd.summer2019.controllers.profiles;

import com.epam.rd.summer2019.domain.Order;
import com.epam.rd.summer2019.dto.InvoiceViewDTO;
import com.epam.rd.summer2019.dto.UserViewDTO;
import com.epam.rd.summer2019.services.InvoiceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class UserProfileController {

    private InvoiceService invoiceService;
    private static final Logger logger = LoggerFactory.getLogger(UserProfileController.class);

    @Autowired
    public UserProfileController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GetMapping(value = "/userProfile")
    public ModelAndView getUserProfile(HttpServletRequest req) {

        HttpSession session = req.getSession();
        UserViewDTO user = (UserViewDTO) session.getAttribute("User");
        List<InvoiceViewDTO> invoiceViewDTOList = invoiceService.getInvoicesByStatusByUser(Order.Status.APPROVED, false, user);
        List<InvoiceViewDTO> invoiceViewDTOListIssue = invoiceService.getInvoicesByStatusByUser(Order.Status.ISSUE, true, user);

        ModelAndView modelAndView = new ModelAndView("/profiles/userProfile");
        modelAndView.addObject("invoiceList", invoiceViewDTOList);
        modelAndView.addObject("invoiceListIssue", invoiceViewDTOListIssue);

        return modelAndView;
    }
}
