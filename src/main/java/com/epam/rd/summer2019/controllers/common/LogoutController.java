package com.epam.rd.summer2019.controllers.common;

import com.epam.rd.summer2019.dto.UserViewDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller
public class LogoutController {

    private static final Logger logger = LoggerFactory.getLogger(LogoutController.class);

    @GetMapping(value = "/logout")
    public ModelAndView getLogout() {
        return new ModelAndView("/login");
    }

    @PostMapping(value = "/logout")
    public void postLogout(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession();
        UserViewDTO user = (UserViewDTO) session.getAttribute("User");
        if (user != null) {
            logger.info(user.getUserName() + " just logout");
            session.invalidate();
        }
        resp.sendRedirect("/login");
    }

}
