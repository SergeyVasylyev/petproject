package com.epam.rd.summer2019.controllers.entitylist;

import com.epam.rd.summer2019.domain.User;
import com.epam.rd.summer2019.dto.InvoiceViewDTO;
import com.epam.rd.summer2019.dto.OrderViewDTO;
import com.epam.rd.summer2019.dto.UserViewDTO;
import com.epam.rd.summer2019.exceptions.ApplicationException;
import com.epam.rd.summer2019.services.InvoiceService;
import com.epam.rd.summer2019.services.OrderService;
import com.epam.rd.summer2019.services.statuses.OrderStatusService;
import com.epam.rd.summer2019.validation.impl.InvoiceValidationImpl;
import com.epam.rd.summer2019.validation.impl.ValidationFactoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import static java.util.Objects.isNull;

@Controller
public class InvoiceController {

    private InvoiceService invoiceService;
    private OrderStatusService statusService;
    private OrderService orderService;
    private static final Logger logger = LoggerFactory.getLogger(InvoiceController.class);

    @Autowired
    public InvoiceController(InvoiceService invoiceService, OrderStatusService statusService, OrderService orderService) {
        this.invoiceService = invoiceService;
        this.statusService = statusService;
        this.orderService = orderService;
    }

    @GetMapping(value = "/invoices")
    public ModelAndView getInvoiceList(HttpServletRequest req) {
        List<InvoiceViewDTO> invoiceList;
        HttpSession session = req.getSession();
        UserViewDTO user = (UserViewDTO) session.getAttribute("User");
        if (user.getUserRole() == User.Role.CLIENT) {
            invoiceList = invoiceService.getInvoicesByUser(user);
        } else {
            invoiceList = invoiceService.getAllInvoices();
        }
        return new ModelAndView("/invoices/invoices", "invoiceList", invoiceList);
    }

    @GetMapping(value = "/invoices/{invoiceId}")
    public ModelAndView viewInvoice(@PathVariable Long invoiceId) {
        InvoiceViewDTO invoiceViewDTO = invoiceService.findInvoice(invoiceId);
        return new ModelAndView("invoices/viewInvoice", "invoiceDTO", invoiceViewDTO);
    }

    @PostMapping(value = "/invoices")
    public void addInvoice(@RequestParam("orderId") Long orderId, @RequestParam("sum") Double sum, @RequestParam("isIssue") String isIssueOrder
            , HttpServletRequest req, HttpServletResponse resp) throws IOException {
        InvoiceValidationImpl validation = ValidationFactoryImpl.getValidationFactory().getInvoiceValidationImpl();
        String validationResult = validation.validate(req);

        OrderViewDTO orderViewDTO = orderService.findOrder(orderId);

        if (validationResult.isEmpty()) {
            InvoiceViewDTO invoiceViewDTO = new InvoiceViewDTO(orderService);
            invoiceViewDTO.setSum(sum);
            invoiceViewDTO.setOrder(orderViewDTO);
            if (!isNull(isIssueOrder) && isIssueOrder.equals("true")) {
                invoiceViewDTO.setStatusIssue(true);
            }
            String action = (invoiceViewDTO.isStatusIssue()) ? "Issue" : "";
            statusService.changeOrderStatus(invoiceViewDTO.getOrder(), action);
            invoiceService.createInvoice(invoiceViewDTO);
            resp.sendRedirect("/invoices");
        } else {
            Locale locale = (Locale) req.getSession().getAttribute("LOCALE");
            ResourceBundle messages = ResourceBundle.getBundle("i18n.messages", locale);

            ModelAndView modelAndView = new ModelAndView("invoices/addInvoice");
            modelAndView.addObject("orderDTO", orderViewDTO);
            modelAndView.addObject("isIssueOrder", isIssueOrder);
            throw new ApplicationException(messages.getString(validationResult), modelAndView);
        }
    }

    @PostMapping(value = "/updateInvoice")
    public String updateInvoice(@ModelAttribute("invoiceViewDTO") InvoiceViewDTO invoiceViewDTO) {
        invoiceService.modifyInvoice(invoiceViewDTO);
        return "redirect: /invoices";
    }

    @PostMapping(value = "/deleteInvoice")
    public String deleteInvoice(@ModelAttribute("invoiceViewDTO") InvoiceViewDTO invoiceViewDTO) {
        invoiceService.deleteInvoice(invoiceViewDTO.getId());
        return "redirect: /invoices";
    }

    //change status for user and manager pages
    @PostMapping(value = "/invoicePayed")
    private String changeInvoiceStatus(@RequestParam("id") Long invoiceId) {
        InvoiceViewDTO invoiceViewDTO = invoiceService.findInvoice(invoiceId);
        statusService.changeOrderStatus(invoiceViewDTO.getOrder(), "");
        return "redirect: /userProfile";
    }

    @PostMapping(value = "/invoiceIssuePayed")
    private String changeInvoiceStatusIssue(@RequestParam("id") Long invoiceId) {
        InvoiceViewDTO invoiceViewDTO = invoiceService.findInvoice(invoiceId);
        statusService.changeOrderStatus(invoiceViewDTO.getOrder(), "Issue");
        return "redirect: /userProfile";
    }

    @PostMapping(value = "/createInvoice")
    private ModelAndView createInvoice(@RequestParam("id") Long orderId) {
        OrderViewDTO orderViewDTO = orderService.findOrder(orderId);
        ModelAndView modelAndView = new ModelAndView("invoices/addInvoice");
        modelAndView.addObject("orderDTO", orderViewDTO);
        return modelAndView;
    }

    @PostMapping(value = "/createInvoiceIssue")
    private ModelAndView createInvoiceIssue(@RequestParam("id") Long orderId) {
        OrderViewDTO orderViewDTO = orderService.findOrder(orderId);
        ModelAndView modelAndView = new ModelAndView("invoices/addInvoice");
        modelAndView.addObject("orderDTO", orderViewDTO);
        modelAndView.addObject("isIssueOrder", "true");
        return modelAndView;
    }
}