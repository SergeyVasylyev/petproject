package com.epam.rd.summer2019.controllers.common;

import com.epam.rd.summer2019.util.UTF8Control;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.ResourceBundle;

@Controller
public class LocaleChange {

    private ResourceBundle.Control utf8Control = new UTF8Control();

    @GetMapping(value = "/changeLocal")
    public ModelAndView getLocaleChange(HttpServletRequest req) {

        String localeFromRequest = req.getParameter("lang");
        if (localeFromRequest != null) {
            Locale locale = new Locale(localeFromRequest);
            req.getSession().setAttribute("LOCALE", locale);
        }
        return new ModelAndView("/login");
    }
}