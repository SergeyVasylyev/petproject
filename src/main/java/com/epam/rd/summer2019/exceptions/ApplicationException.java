package com.epam.rd.summer2019.exceptions;

import org.springframework.web.servlet.ModelAndView;

public class ApplicationException extends RuntimeException {

    public ModelAndView getModelAndView() {
        return modelAndView;
    }

    private ModelAndView modelAndView;

    public ApplicationException(String message) {
        super(message);
    }

    public ApplicationException(String message, ModelAndView modelAndView) {
        super(message);
        this.modelAndView = modelAndView;
    }
    public ApplicationException(String message, Throwable cause) {
        super(message, cause);
    }
}
