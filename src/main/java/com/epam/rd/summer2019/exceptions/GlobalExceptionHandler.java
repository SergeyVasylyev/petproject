package com.epam.rd.summer2019.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler({UserNotFoundException.class, ApplicationException.class})
    public final ModelAndView handleException(HttpServletRequest req, HttpServletResponse resp, Exception ex) throws Exception {

        ModelAndView modelAndView = new ModelAndView();

        if (ex instanceof UserNotFoundException) {
            logger.error("UserNotFoundException. " + ex.getMessage());
            req.getSession().setAttribute("errorLoginPassMessage", ex.getMessage());
            resp.sendRedirect("/jsp/login.jsp");
        } else if (ex instanceof ApplicationException) {
            logger.error("ApplicationException. " + ex.getMessage());
            modelAndView = ((ApplicationException) ex).getModelAndView();
            modelAndView.addObject("validationError", ex.getMessage());
        } else {
            logger.error("Unexpected exception. " + ex.getMessage());
            req.getSession().setAttribute("errorLoginPassMessage", "Unexpected error. Error message has been sent to the support group!");
            resp.sendRedirect("/jsp/login.jsp");
        }

        return modelAndView;
    }
}