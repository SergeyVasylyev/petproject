package com.epam.rd.summer2019.validation.impl;

import com.epam.rd.summer2019.validation.ValidationService;

import javax.servlet.http.HttpServletRequest;

public class InvoiceValidationImpl implements ValidationService<HttpServletRequest> {
    @Override
    public String validate(HttpServletRequest req) {

        Double sum = Double.parseDouble(req.getParameter("sum"));

        StringBuilder validationSB = new StringBuilder();
        if (!validateSum(sum)) validationSB.append("ValidateSum");

        return validationSB.toString();
    }

    private boolean validateSum(double sum) {
        return sum > 0;
    }
}