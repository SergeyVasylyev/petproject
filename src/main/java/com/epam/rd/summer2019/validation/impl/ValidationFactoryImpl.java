package com.epam.rd.summer2019.validation.impl;

import com.epam.rd.summer2019.validation.ValidationFactory;

public class ValidationFactoryImpl implements ValidationFactory {

    private static final ValidationFactoryImpl validatorFactory = new ValidationFactoryImpl();

    private final CarValidationImpl carValidation = new CarValidationImpl();
    private final OrderValidationImpl orderValidation = new OrderValidationImpl();
    private final InvoiceValidationImpl invoiceValidation = new InvoiceValidationImpl();

    public static ValidationFactoryImpl getValidationFactory() {
        return validatorFactory;
    }

    @Override
    public CarValidationImpl getCarValidationImpl() {
        return this.carValidation;
    }

    @Override
    public OrderValidationImpl getOrderValidationImpl() {
        return this.orderValidation;
    }

    @Override
    public InvoiceValidationImpl getInvoiceValidationImpl() {
        return this.invoiceValidation;
    }
}
