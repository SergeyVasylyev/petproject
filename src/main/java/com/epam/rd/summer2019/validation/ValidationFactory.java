package com.epam.rd.summer2019.validation;

import com.epam.rd.summer2019.validation.impl.CarValidationImpl;
import com.epam.rd.summer2019.validation.impl.InvoiceValidationImpl;
import com.epam.rd.summer2019.validation.impl.OrderValidationImpl;

public interface ValidationFactory<Validator> {

    CarValidationImpl getCarValidationImpl();

    OrderValidationImpl getOrderValidationImpl();

    InvoiceValidationImpl getInvoiceValidationImpl();
}
