package com.epam.rd.summer2019.validation;

public interface ValidationService<HttpServletRequest> {
    String validate(javax.servlet.http.HttpServletRequest request);
}
