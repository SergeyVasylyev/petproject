package com.epam.rd.summer2019.validation.impl;

import com.epam.rd.summer2019.validation.ValidationService;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OrderValidationImpl implements ValidationService<HttpServletRequest> {

    public static final String Regex = "\\D{2}\\d{6}";

    @Override
    public String validate(HttpServletRequest req) {

        String passport = req.getParameter("passport");
        int days = Integer.parseInt(req.getParameter("days"));

        StringBuilder validationSB = new StringBuilder();
        if (!validatePassport(passport)) validationSB.append("ValidationPassport");
        if (!validateDays(days)) validationSB.append("ValidationDays");

        return validationSB.toString();
    }

    private boolean validatePassport(String quality) {
        Pattern p = Pattern.compile(Regex);
        Matcher m = p.matcher(quality);
        return m.matches();
    }

    private boolean validateDays(int days) {
        return (days >= 1 && days <= 30);
    }
}

