package com.epam.rd.summer2019.validation.impl;

import com.epam.rd.summer2019.domain.Car;
import com.epam.rd.summer2019.validation.ValidationService;

import javax.servlet.http.HttpServletRequest;

public class CarValidationImpl implements ValidationService<HttpServletRequest> {

    @Override
    public String validate(HttpServletRequest req) {

        String quality = req.getParameter("quality");
        Double rentPrice = Double.parseDouble(req.getParameter("rentPrice"));

        StringBuilder validationSB = new StringBuilder();
        if (!validateQuality(quality)) validationSB.append("ValidateQuality");
        if (!validateRentPrice(rentPrice)) validationSB.append("ValidateRentPrice");

        return validationSB.toString();
    }

    private boolean validateQuality(String quality) {
        Car.QualityClass[] mas = Car.QualityClass.values();
        for (Car.QualityClass carClass : mas) {
            if (String.valueOf(carClass).compareTo(quality) == 0) return true;
        }
        return false;
    }

    private boolean validateRentPrice(double rentPrice) {
        return rentPrice >= 1 && rentPrice <= 1000;
    }
}
