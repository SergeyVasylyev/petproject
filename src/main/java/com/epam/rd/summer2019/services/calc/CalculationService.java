package com.epam.rd.summer2019.services.calc;

import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class CalculationService {

    public static final int DRIVER_INCLUDED_SUM = 101;

    public double getSum(HttpServletRequest req) {
        double sum = 0;

        String daysString = req.getParameter("days");
        String rentPriceString = req.getParameter("rentPrice");
        String driverIncludedString = req.getParameter("driverIncluded");
        if (driverIncludedString == null) driverIncludedString = "";
        if (driverIncludedString.equals("on")) {
            sum = DRIVER_INCLUDED_SUM;
        }

        sum += Double.parseDouble(rentPriceString) * Integer.parseInt(daysString);

        return sum;
    }
}
