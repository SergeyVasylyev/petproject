package com.epam.rd.summer2019.services.impl;

import com.epam.rd.summer2019.converter.UserDTOConverter;
import com.epam.rd.summer2019.dao.UserDao;
import com.epam.rd.summer2019.domain.User;
import com.epam.rd.summer2019.dto.UserViewDTO;
import com.epam.rd.summer2019.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private UserDao userDao;
    private UserDTOConverter userDTOConverter;
    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    public UserServiceImpl(UserDao userDao, UserDTOConverter userDTOConverter) {
        this.userDao = userDao;
        this.userDTOConverter = userDTOConverter;
    }

    @Override
    public void createUser(UserViewDTO userDTO) {
        User user = userDTOConverter.convertDTOToUser(userDTO);
        String userEmail = user.getEmail();
        User tempUser = userDao.findUser(userEmail);
        if (tempUser != null) {
            logger.error("Trying to add existing user. User exist! " + tempUser.toString());
            return;
        }
        userDao.saveUser(user);
    }

    @Override
    public void deleteUser(Long id) {
        User user = userDao.findUser(id);
        if (user != null) {
            userDao.deleteUser(user);
        }
    }

    @Override
    public UserViewDTO findUser(Long id) {
        User user = userDao.findUser(id);
        showUser(id, user);
        if (user != null) {
            return userDTOConverter.convertUserToDTO(user);
        }
        return null;
    }

    @Override
    public UserViewDTO findUser(String email) {
        User user = userDao.findUser(email);
        if (user != null) {
            showUser(user.getUserId(), user);
            return userDTOConverter.convertUserToDTO(user);
        }
        return null;
    }

    @Override
    public void modifyUser(UserViewDTO userDTO) {
        User user = userDTOConverter.convertDTOToUser(userDTO);
        User tempUser = userDao.findUser(user.getUserId());
        if (tempUser != null) {
            userDao.modifyUser(user);
        } else {
            logger.error("Can't find user with id=0. User data: " + userDTO.toString());
        }
    }

    @Override
    public List<UserViewDTO> GetAllUsers() {
        List<User> userList = userDao.getUserList();
        List<UserViewDTO> userViewDTO = userList.stream()
                .map(userDTOConverter::convertUserToDTO)
                .collect(Collectors.toList());
        return userViewDTO;
    }

    private void showUser(Long id, User user) {
        String userResult;
        if (user != null) {
            userResult = "Found user: " + user;
        } else {
            userResult = "User not found: " + id;
        }
        //logger.error(userResult);
        logger.info(userResult);
    }
}
