package com.epam.rd.summer2019.services;

import com.epam.rd.summer2019.domain.Order;
import com.epam.rd.summer2019.dto.OrderViewDTO;
import com.epam.rd.summer2019.dto.UserViewDTO;

import java.util.List;

public interface OrderService {

    void createOrder(OrderViewDTO order);

    void deleteOrder(Long id);

    OrderViewDTO findOrder(Long id);

    void modifyOrder(OrderViewDTO order);

    List<OrderViewDTO> getAllOrders();

    List<OrderViewDTO> getOrdersByStatus(Order.Status status);

    List<OrderViewDTO> getOrdersByUser(UserViewDTO user);

}
