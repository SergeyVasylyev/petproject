package com.epam.rd.summer2019.services.impl;

import com.epam.rd.summer2019.converter.OrderDTOConverter;
import com.epam.rd.summer2019.dao.OrderDao;
import com.epam.rd.summer2019.domain.Order;
import com.epam.rd.summer2019.dto.OrderViewDTO;
import com.epam.rd.summer2019.dto.UserViewDTO;
import com.epam.rd.summer2019.services.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

    private OrderDao orderDao;
    private OrderDTOConverter orderDtoConverter;
    private static final Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

    @Autowired
    public OrderServiceImpl(OrderDao orderDao, OrderDTOConverter orderDtoConverter) {
        this.orderDao = orderDao;
        this.orderDtoConverter = orderDtoConverter;
    }

    @Override
    public void createOrder(OrderViewDTO orderDTO) {
        Order order = orderDtoConverter.convertDTOToOrder(orderDTO);
        orderDao.saveOrder(order);
    }

    @Override
    public void deleteOrder(Long id) {
        Order order = orderDao.findOrder(id);
        if (order != null) {
            orderDao.deleteOrder(order);
        }
    }

    @Override
    public OrderViewDTO findOrder(Long id) {
        Order order = orderDao.findOrder(id);
        showOrder(id, order);
        return orderDtoConverter.convertOrderToDTO(order);
    }

    @Override
    public List<OrderViewDTO> getAllOrders() {
        List<Order> orderList = orderDao.getOrderList();
        List<OrderViewDTO> orderViewDTOList = orderList.stream()
                .map(orderDtoConverter::convertOrderToDTO)
                .collect(Collectors.toList());
        return orderViewDTOList;
    }

    @Override
    public void modifyOrder(OrderViewDTO orderDTO) {
        Order order = orderDtoConverter.convertDTOToOrder(orderDTO);
        Order tempOrder = orderDao.findOrder(order.getId());
        if (tempOrder != null) {
            orderDao.modifyOrder(order);
        }
    }

    @Override
    public List<OrderViewDTO> getOrdersByStatus(Order.Status status) {
        List<OrderViewDTO> orderViewDTOList = getAllOrders();
        orderViewDTOList = orderViewDTOList.stream()
                .filter(o -> o.getStatus() == status)
                .collect(Collectors.toList());
        return orderViewDTOList;
    }

    @Override
    public List<OrderViewDTO> getOrdersByUser(UserViewDTO user) {
        List<OrderViewDTO> orderViewDTOList = getAllOrders();
        orderViewDTOList = orderViewDTOList.stream()
                .filter(o -> o.getUser().equals(user))
                .collect(Collectors.toList());
        return orderViewDTOList;
    }

    private void showOrder(Long id, Order order) {
        String orderResult;
        if (order != null) {
            orderResult = "Found order: " + order;
        } else {
            orderResult = "Order not found: " + id;
        }
        logger.info(orderResult);
    }
}
