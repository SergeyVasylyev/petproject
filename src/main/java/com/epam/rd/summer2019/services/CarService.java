package com.epam.rd.summer2019.services;

import com.epam.rd.summer2019.dto.CarViewDTO;

import java.util.List;

public interface CarService {

    void createCar(CarViewDTO carViewDTO);

    void modifyCar(CarViewDTO carViewDTO);

    CarViewDTO findCar(Long id);

    void deleteCar(Long id);

    List<CarViewDTO> getAllCars();

    List<CarViewDTO> getAllCars(boolean available);

    List<CarViewDTO> getAllCarsFilterSort(String filter1, String filter2, String sortType, boolean available);
}
