package com.epam.rd.summer2019.services.impl;

import com.epam.rd.summer2019.converter.InvoiceDTOConverter;
import com.epam.rd.summer2019.dao.InvoiceDao;
import com.epam.rd.summer2019.domain.Invoice;
import com.epam.rd.summer2019.domain.Order;
import com.epam.rd.summer2019.dto.InvoiceViewDTO;
import com.epam.rd.summer2019.dto.UserViewDTO;
import com.epam.rd.summer2019.services.InvoiceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class InvoiceServiceImpl implements InvoiceService {

    private InvoiceDao invoiceDao;
    private InvoiceDTOConverter invoiceDTOConverter;
    private static final Logger logger = LoggerFactory.getLogger(InvoiceServiceImpl.class);

    @Autowired
    public InvoiceServiceImpl(InvoiceDao invoiceDao, InvoiceDTOConverter invoiceDTOConverter) {
        this.invoiceDao = invoiceDao;
        this.invoiceDTOConverter = invoiceDTOConverter;
    }

    @Override
    public void createInvoice(InvoiceViewDTO invoiceViewDTO) {
        Invoice invoice = invoiceDTOConverter.convertDTOToInvoice(invoiceViewDTO);
        logger.info("Create new invoice " + invoiceViewDTO.toString());
        invoiceDao.saveInvoice(invoice);
    }

    @Override
    public void deleteInvoice(Long id) {
        Invoice invoice = invoiceDao.findInvoice(id);
        if (invoice != null) {
            invoiceDao.deleteInvoice(invoice);
        }
    }

    @Override
    public InvoiceViewDTO findInvoice(Long id) {
        Invoice invoice = invoiceDao.findInvoice(id);
        showInvoice(id, invoice);
        return invoiceDTOConverter.convertInvoiceToDTO(invoice);
    }

    @Override
    public void modifyInvoice(InvoiceViewDTO invoiceViewDTO) {
        Invoice invoice = invoiceDTOConverter.convertDTOToInvoice(invoiceViewDTO);
        Invoice tempInvoice = invoiceDao.findInvoice(invoice.getId());
        if (tempInvoice != null) {
            invoiceDao.modifyInvoice(invoice);
        }
    }

    @Override
    public List<InvoiceViewDTO> getAllInvoices() {
        List<Invoice> invoiceList = invoiceDao.getInvoiceList();
        List<InvoiceViewDTO> invoiceViewDTO = invoiceList.stream()
                .map(invoiceDTOConverter::convertInvoiceToDTO)
                .collect(Collectors.toList());
        return invoiceViewDTO;
    }

    @Override
    public List<InvoiceViewDTO> getInvoicesByStatus(Order.Status status) {
        List<InvoiceViewDTO> invoiceViewDTOList = getAllInvoices();
        invoiceViewDTOList = invoiceViewDTOList.stream()
                .filter(inv -> inv.getOrder().getStatus() == status)
                .collect(Collectors.toList());
        return invoiceViewDTOList;
    }

    @Override
    public List<InvoiceViewDTO> getInvoicesByUser(UserViewDTO user) {
        List<InvoiceViewDTO> invoiceViewDTOList = getAllInvoices();
        invoiceViewDTOList = invoiceViewDTOList.stream()
                .filter(inv -> inv.getOrder().getUser().equals(user))
                .collect(Collectors.toList());
        return invoiceViewDTOList;
    }

    @Override
    public List<InvoiceViewDTO> getInvoicesByStatusByUser(Order.Status status, boolean isIssue, UserViewDTO user) {
        List<InvoiceViewDTO> invoiceViewDTOList = getAllInvoices();
        invoiceViewDTOList = invoiceViewDTOList.stream()
                .filter(inv -> inv.getOrder().getUser().equals(user) && inv.isStatusIssue() == isIssue && inv.getOrder().getStatus() == status)
                .collect(Collectors.toList());
        return invoiceViewDTOList;
    }

    private void showInvoice(Long id, Invoice invoice) {
        String invoiceResult;
        if (invoice != null) {
            invoiceResult = "Found invoice: " + invoice;
        } else {
            invoiceResult = "Invoice not found: " + id;
        }
        logger.info(invoiceResult);
    }
}
