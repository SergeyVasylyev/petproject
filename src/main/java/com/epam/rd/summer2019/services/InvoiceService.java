package com.epam.rd.summer2019.services;

import com.epam.rd.summer2019.domain.Order;
import com.epam.rd.summer2019.dto.InvoiceViewDTO;
import com.epam.rd.summer2019.dto.UserViewDTO;

import java.util.List;

public interface InvoiceService {

    void createInvoice(InvoiceViewDTO invoiceViewDTO);

    void deleteInvoice(Long id);

    InvoiceViewDTO findInvoice(Long id);

    void modifyInvoice(InvoiceViewDTO invoiceViewDTO);

    List<InvoiceViewDTO> getAllInvoices();

    List<InvoiceViewDTO> getInvoicesByStatus(Order.Status status);

    List<InvoiceViewDTO> getInvoicesByUser(UserViewDTO user);

    List<InvoiceViewDTO> getInvoicesByStatusByUser(Order.Status status, boolean isIssue, UserViewDTO user);
}
