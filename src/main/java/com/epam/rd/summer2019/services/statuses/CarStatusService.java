package com.epam.rd.summer2019.services.statuses;

import com.epam.rd.summer2019.domain.Order;
import com.epam.rd.summer2019.dto.CarViewDTO;
import com.epam.rd.summer2019.dto.OrderViewDTO;
import com.epam.rd.summer2019.services.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CarStatusService {

    private CarService carService;

    @Autowired
    public CarStatusService(CarService carService) {
        this.carService = carService;
    }

    public void setCarStatusService(CarService carService) {
        this.carService = carService;
    }

    public void changeCarStatus(OrderViewDTO orderViewDTO) {

        CarViewDTO carViewDTO = orderViewDTO.getCar();

        if (orderViewDTO.getStatus() == Order.Status.NEW) {
            carViewDTO.setAvailable(false);
        } else if (orderViewDTO.getStatus() == Order.Status.REJECTED
                || orderViewDTO.getStatus() == Order.Status.CLOSED) {
            carViewDTO.setAvailable(true);
        }
        carService.modifyCar(carViewDTO);
    }
}
