package com.epam.rd.summer2019.services.statuses;

import com.epam.rd.summer2019.domain.Order;
import com.epam.rd.summer2019.dto.OrderViewDTO;
import com.epam.rd.summer2019.services.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.epam.rd.summer2019.domain.Order.Status.*;

@Service
public class OrderStatusService {

    private OrderService orderService;
    private CarStatusService carStatusService;
    private static final Logger logger = LoggerFactory.getLogger(OrderStatusService.class);

    @Autowired
    public OrderStatusService(OrderService orderService, CarStatusService carStatusService) {
        this.orderService = orderService;
        this.carStatusService = carStatusService;
    }

    public void setCarStatusService(CarStatusService carStatusService) {
        this.carStatusService = carStatusService;
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public void changeOrderStatus(OrderViewDTO order, String action) {
        switch (order.getStatus()) {
            case NEW:
                if (action.equals("Reject")) {
                    setOrderStatus(order, REJECTED);
                } else {
                    setOrderStatus(order, APPROVED);
                }
                break;
            case APPROVED:
                setOrderStatus(order, PAYED);
                break;
            case PAYED:
                if (action.equals("Issue")) {
                    setOrderStatus(order, ISSUE);
                } else {
                    setOrderStatus(order, CLOSED);
                }
                break;
            case ISSUE:
                setOrderStatus(order, CLOSED);
                break;
            default:
                break;
        }
        carStatusService.changeCarStatus(order);
    }

    public void setOrderStatus(OrderViewDTO order, Order.Status status) {
        order.setStatus(status);
        orderService.modifyOrder(order);
    }

}
