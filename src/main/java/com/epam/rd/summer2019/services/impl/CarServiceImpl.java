package com.epam.rd.summer2019.services.impl;

import com.epam.rd.summer2019.converter.CarDTOConverter;
import com.epam.rd.summer2019.dao.CarDao;
import com.epam.rd.summer2019.domain.Car;
import com.epam.rd.summer2019.dto.CarViewDTO;
import com.epam.rd.summer2019.services.CarService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarServiceImpl implements CarService {

    private CarDao carDao;
    private CarDTOConverter carDTOConverter;
    private static final Logger logger = LoggerFactory.getLogger(CarServiceImpl.class);

    @Autowired
    public CarServiceImpl(CarDao carDao, CarDTOConverter carDTOConverter) {
        this.carDao = carDao;
        this.carDTOConverter = carDTOConverter;
    }

    @Override
    public void createCar(CarViewDTO carViewDTO) {
        Car car = carDTOConverter.convertDTOToCar(carViewDTO);
        Car tempCar = carDao.findCar(car.getNumber());
        if (tempCar != null) {
            logger.error("Trying to add existing car. Car exist! " + tempCar.toString());
            return;
        }
        if (car.getId() == 0) {
            car.setAvailable(true);
        }
        carDao.saveCar(car);
    }

    @Override
    public void modifyCar(CarViewDTO carViewDTO) {
        Car car = carDTOConverter.convertDTOToCar(carViewDTO);
        Car tempCar = carDao.findCar(car.getId());
        if (tempCar != null) {
            carDao.modifyCar(car);
        }
    }

    @Override
    public CarViewDTO findCar(Long id) {
        Car car = carDao.findCar(id);
        showCar(id, car);
        return carDTOConverter.convertCarToDTO(car);
    }

    @Override
    public void deleteCar(Long id) {
        Car tempCar = carDao.findCar(id);
        if (tempCar != null) {
            carDao.deleteCar(tempCar);
        }
    }

    @Override
    public List<CarViewDTO> getAllCars() {
        List<Car> carList = carDao.getCarsList();
        List<CarViewDTO> carViewDTOList = carList.stream()
                .map(carDTOConverter::convertCarToDTO)
                .collect(Collectors.toList());
        return carViewDTOList;
    }

    @Override
    public List<CarViewDTO> getAllCars(boolean available) {
        List<Car> carList = carDao.getCarsList();
        List<CarViewDTO> carViewDTOList = carList.stream()
                .map(carDTOConverter::convertCarToDTO)
                .filter(car -> !available || car.isAvailable())
                .collect(Collectors.toList());
        return carViewDTOList;
    }

    @Override
    public List<CarViewDTO> getAllCarsFilterSort(String filterType, String filterQuality, String sortType, boolean available) {

        Comparator compare;
        if (sortType.equals("sortByRentPrice")) {
            compare = Comparator.comparingDouble(CarViewDTO::getRentPrice);
        } else {
            compare = Comparator.comparing(CarViewDTO::getName);
        }

        Car.QualityClass qualityClass = !filterQuality.isEmpty() ? Car.QualityClass.valueOf(filterQuality) : null;
        List<Car> carList = carDao.getCarsList();
        List<CarViewDTO> carViewDTOList = (List<CarViewDTO>) carList.stream()
                .map(carDTOConverter::convertCarToDTO)
                .filter(car -> filterType.isEmpty() || car.getType().equals(filterType))
                .filter(car -> filterQuality.isEmpty() || car.getQuality() == qualityClass)
                .filter(car -> !available || car.isAvailable())
                .sorted(compare)
                .collect(Collectors.toList());

        return carViewDTOList;
    }

    static void showCar(Long id, Car car) {
        String carResult;
        if (car != null) {
            //carResult = "Found car: " + car;
        } else {
            carResult = "Car not found: " + id;
            logger.info(carResult);
        }
    }
}
