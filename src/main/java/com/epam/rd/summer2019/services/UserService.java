package com.epam.rd.summer2019.services;

import com.epam.rd.summer2019.dto.UserViewDTO;

import java.util.List;

public interface UserService {

    void createUser(UserViewDTO userDTO);

    void deleteUser(Long id);

    UserViewDTO findUser(Long id);

    UserViewDTO findUser(String email);

    void modifyUser(UserViewDTO userDTO);

    List<UserViewDTO> GetAllUsers();

}
