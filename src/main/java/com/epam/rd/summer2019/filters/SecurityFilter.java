package com.epam.rd.summer2019.filters;

import com.epam.rd.summer2019.config.SecurityConfig;
import com.epam.rd.summer2019.domain.User;
import com.epam.rd.summer2019.dto.UserViewDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;


@WebFilter(urlPatterns = {"/*"}, initParams = {@WebInitParam(name = "LOGIN_PATH", value = "/jsp/login.jsp")})
public class SecurityFilter implements Filter {

    private String loginPath;

    private SecurityConfig config = new SecurityConfig();
    private static final Logger logger = LoggerFactory.getLogger(SecurityFilter.class);

    public void init(FilterConfig fConfig) throws ServletException {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        UserViewDTO user = (UserViewDTO) httpRequest.getSession().getAttribute("User");
        String url = httpRequest.getRequestURL().toString();
        if (user != null) {
            User.Role role = user.getUserRole();
            if (user.getUserStatus() == User.UserStatus.BLOCKED && shouldBeProtectedWithoutRole(url) || shouldBeProtected(role, url)) {
                httpResponse.sendRedirect("/login");
            }
        } else {
            if (shouldBeProtectedWithoutRole(url)) {
                httpResponse.sendRedirect("/login");
            }
        }

        chain.doFilter(request, response);
    }

    public void destroy() {
        loginPath = null;
    }

    public boolean shouldBeProtected(User.Role role, String url) {
        Map<User.Role, List<String>> protectedURLs = config.getProtectedURLs();
        List<String> urls = protectedURLs.get(role);
        if (urls != null) {
            return urls.stream().anyMatch(url::endsWith);
        }
        return false;
    }

    public boolean shouldBeProtectedWithoutRole(String url) {
        List<String> protectedURLs = config.getProtectedURLsWithoutRole();
        return protectedURLs.stream().anyMatch(url::contains);
    }
}
