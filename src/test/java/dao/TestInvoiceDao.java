package dao;

import com.epam.rd.summer2019.dao.InvoiceDao;
import com.epam.rd.summer2019.dao.UserDao;
import com.epam.rd.summer2019.dao.impl.CarDaoImpl;
import com.epam.rd.summer2019.dao.impl.InvoiceDaoImpl;
import com.epam.rd.summer2019.dao.impl.OrderDaoImpl;
import com.epam.rd.summer2019.dao.impl.UserDaoImpl;
import com.epam.rd.summer2019.dao.utils.ConnectionH2;
import com.epam.rd.summer2019.domain.Car;
import com.epam.rd.summer2019.domain.Invoice;
import com.epam.rd.summer2019.domain.Order;
import com.epam.rd.summer2019.domain.User;
import org.junit.*;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;

public class TestInvoiceDao {

    private CarDaoImpl carDao;
    private OrderDaoImpl orderDao;
    private UserDao userDao;
    private InvoiceDao invoiceDao;
    private ConnectionH2 connectionUtils;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void init(){
        connectionUtils = new ConnectionH2();
        connectionUtils.prepareDb();
        orderDao = new OrderDaoImpl(connectionUtils);
        carDao = new CarDaoImpl(connectionUtils);
        userDao = new UserDaoImpl(connectionUtils);
        invoiceDao = new InvoiceDaoImpl(connectionUtils);
    }

    @After
    public void tearDown(){
        List<Invoice> invoiceList = invoiceDao.getInvoiceList();
        for (Invoice invoice: invoiceList) invoiceDao.deleteInvoice(invoice);
        connectionUtils.stopDb();
        orderDao = null;
        carDao = null;
        userDao = null;
        invoiceDao = null;
        connectionUtils = null;
    }

    @Test
    public void testCreateInvoice_NotNull_CorrectId() {
        //GIVEN
        String carNumber = "carNumber";
        Car car = new Car.Builder(carNumber).quality(Car.QualityClass.B).build();
        carDao.saveCar(car);
        car = carDao.findCar(carNumber);

        User user = new User(1,"firstName","lastName","userName","password","email",User.Role.CLIENT, User.UserStatus.ACTIVE);
        userDao.saveUser(user);

        String orderCode = "orderCode";
        Order order = new Order.Builder(1, orderCode,"WE2233", car).status(Order.Status.APPROVED).user(user).build();
        orderDao.saveOrder(order);

        String invoiceCode = "invoiceCode";
        Invoice invoice = new Invoice.Builder(invoiceCode, order).build();
        invoiceDao.saveInvoice(invoice);

        //WHEN
        Invoice resultInvoice = invoiceDao.findInvoice(invoiceCode);

        //THEN
        Assert.assertNotNull(resultInvoice);
        Assert.assertThat(resultInvoice.getCode(), is(invoice.getCode()));
    }

    @Test
    public void testModifyInvoice_NotNull_ChangeData() {
        //GIVEN
        String carNumber = "carNumber";
        Car car = new Car.Builder(carNumber).quality(Car.QualityClass.B).build();
        carDao.saveCar(car);
        car = carDao.findCar(carNumber);

        User user = new User(1,"firstName","lastName","userName","password","email",User.Role.CLIENT, User.UserStatus.ACTIVE);
        userDao.saveUser(user);

        String orderCode = "orderCode";
        Order order = new Order.Builder(1, orderCode,"WE2233", car).status(Order.Status.APPROVED).user(user).build();
        orderDao.saveOrder(order);

        String invoiceCode = "invoiceCode";
        String invoiceCodeNew = "invoiceCodeNew";
        Invoice invoice = new Invoice.Builder(invoiceCode, order).build();
        invoiceDao.saveInvoice(invoice);
        invoice.setCode(invoiceCodeNew);
        invoiceDao.saveInvoice(invoice);

        //WHEN
        Invoice resultInvoice = invoiceDao.findInvoice(invoiceCodeNew);

        //THEN
        Assert.assertNotNull(resultInvoice);
        Assert.assertThat(resultInvoice.getCode(), is(invoice.getCode()));
    }

    @Test
    public void testGetAllInvoices_CorrectSize() {
        String carNumber = "carNumber";
        Car car = new Car.Builder(carNumber).quality(Car.QualityClass.B).build();
        carDao.saveCar(car);
        car = carDao.findCar(carNumber);

        User user = new User(1,"firstName","lastName","userName","password","email",User.Role.CLIENT, User.UserStatus.ACTIVE);
        userDao.saveUser(user);

        String orderCode = "orderCode";
        Order order = new Order.Builder(1, orderCode,"WE2233", car).status(Order.Status.APPROVED).user(user).build();
        orderDao.saveOrder(order);

        String invoiceCode = "invoiceCode";
        Invoice invoice = new Invoice.Builder(invoiceCode, order).build();

        List<Invoice> sourceList = new ArrayList<>();
        sourceList.add(invoice);
        sourceList.add(invoice);
        sourceList.add(invoice);
        for (Invoice invoiceToAdd : sourceList) {
            invoiceDao.saveInvoice(invoiceToAdd);
        }
        int expectedSize = sourceList.size();

        //WHEN
        List<Invoice> resultListInvoice = invoiceDao.getInvoiceList();

        //THEN
        Assert.assertNotNull(resultListInvoice);
        Assert.assertThat(resultListInvoice.size(), is(expectedSize));
    }

    @Test
    public void testDeleteInvoice_InvoiceNotExist() {
        //GIVEN
        String carNumber = "carNumber";
        Car car = new Car.Builder(carNumber).quality(Car.QualityClass.B).build();
        carDao.saveCar(car);
        car = carDao.findCar(carNumber);

        User user = new User(1,"firstName","lastName","userName","password","email",User.Role.CLIENT, User.UserStatus.ACTIVE);
        userDao.saveUser(user);

        String orderCode = "orderCode";
        Order order = new Order.Builder(1, orderCode,"WE2233", car).status(Order.Status.APPROVED).user(user).build();
        orderDao.saveOrder(order);

        String invoiceCode = "invoiceCode";
        Invoice invoice = new Invoice.Builder(invoiceCode, order).build();
        invoiceDao.saveInvoice(invoice);
        invoice = invoiceDao.findInvoice(invoiceCode);
        invoiceDao.deleteInvoice(invoice);

        //WHEN
        Invoice resultInvoice = invoiceDao.findInvoice(invoiceCode);

        //THEN
        Assert.assertNull(resultInvoice);
    }
}
