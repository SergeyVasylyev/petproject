package dao;

import com.epam.rd.summer2019.dao.UserDao;
import com.epam.rd.summer2019.dao.impl.UserDaoImpl;
import com.epam.rd.summer2019.dao.utils.ConnectionH2;
import com.epam.rd.summer2019.domain.User;
import org.junit.*;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;

public class TestUserDao {

    private UserDao userDao;
    private ConnectionH2 connectionUtils;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void init(){
        connectionUtils = new ConnectionH2();
        connectionUtils.prepareDb();
        userDao = new UserDaoImpl(connectionUtils);
    }

    @After
    public void tearDown(){
        List<User> userList = userDao.getUserList();
        for (User user: userList) userDao.deleteUser(user);
        connectionUtils.stopDb();
        userDao = null;
        connectionUtils = null;
    }

    @Test
    public void testCreateUser_NotNull_CorrectId() {
        //GIVEN
        String email = "email";
        User user = new User(1,"firstName","lastName","userName","password", email,User.Role.CLIENT, User.UserStatus.ACTIVE);
        userDao.saveUser(user);

        //WHEN
        User resultUser = userDao.findUser(email);

        //THEN
        Assert.assertNotNull(resultUser);
        Assert.assertThat(resultUser.getEmail(), is(user.getEmail()));
    }

    @Test
    public void testModifyUser_NotNull_ChangeData() {
        //GIVEN
        String email = "email";
        String emailNew = "emailNew";
        User user = new User(1,"firstName","lastName","userName","password", email, User.Role.CLIENT, User.UserStatus.ACTIVE);
        userDao.saveUser(user);
        user.setEmail(emailNew);
        userDao.saveUser(user);

        //WHEN
        User resultUser = userDao.findUser(emailNew);

        //THEN
        Assert.assertNotNull(resultUser);
        Assert.assertThat(resultUser.getEmail(), is(user.getEmail()));
    }

    @Test
    public void testGetAllUsers_CorrectSize() {
        //GIVEN
        //clear data from default upload
        List<User> userList = userDao.getUserList();
        for (User user: userList) userDao.deleteUser(user);

        String email = "email";
        User user = new User(1,"firstName","lastName","userName","password", email, User.Role.CLIENT, User.UserStatus.ACTIVE);

        userList = new ArrayList<>();
        userList.add(user);
        userList.add(user);
        userList.add(user);
        for (User userToAdd: userList) userDao.saveUser(userToAdd);

        //WHEN
        List<User> resultUserList = userDao.getUserList();

        //THEN
        Assert.assertNotNull(resultUserList);
        Assert.assertThat(userList.size(), is(resultUserList.size()));
    }

    @Test
    public void testDeleteUser_NotExist() {
        //GIVEN
        String email = "email";
        User user = new User(1,"firstName","lastName","userName","password", email, User.Role.CLIENT, User.UserStatus.ACTIVE);
        userDao.saveUser(user);
        user = userDao.findUser(email);
        userDao.deleteUser(user);

        //WHEN
        User resultUser = userDao.findUser(email);

        //THEN
        Assert.assertNull(resultUser);
    }
}
