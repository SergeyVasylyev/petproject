package dao;

import com.epam.rd.summer2019.dao.impl.CarDaoImpl;
import com.epam.rd.summer2019.dao.utils.ConnectionH2;
import com.epam.rd.summer2019.domain.Car;
import org.junit.*;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;

public class TestCarDao {

    private CarDaoImpl carDao;
    private ConnectionH2 connectionUtils;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void init(){
        connectionUtils = new ConnectionH2();
        connectionUtils.prepareDb();
        carDao = new CarDaoImpl(connectionUtils);
    }

    @After
    public void tearDown(){
        List<Car> carList = carDao.getCarsList();
        for (Car car: carList) carDao.deleteCar(car);
        connectionUtils.stopDb();
        carDao = null;
        connectionUtils = null;
    }

    @Test
    public void testCreateCar_NotNull_CorrectNumber() {
        //GIVEN
        String carNumber = "carnumber";
        Car car = new Car.Builder(carNumber)
                .type("Audi")
                .name("Q7")
                .quality(Car.QualityClass.B)
                .build();
        carDao.saveCar(car);

        //WHEN
        Car resultCar = carDao.findCar(carNumber);

        //THEN
        Assert.assertNotNull(resultCar);
        Assert.assertThat(resultCar.getNumber(), is(car.getNumber()));
    }

    @Test
    public void testModifyCar_NotNull_ChangeData() {
        //GIVEN
        String carNumber = "carnumber";
        String carNumberNew = "carnumberNew";
        Car car = new Car.Builder(carNumber)
                .type("Audi")
                .name("Q7")
                .quality(Car.QualityClass.B)
                .build();
        carDao.saveCar(car);
        car = carDao.findCar(carNumber);
        car.setNumber(carNumberNew);
        carDao.modifyCar(car);

        //WHEN
        Car resultCar = carDao.findCar(carNumberNew);

        //THEN
        Assert.assertNotNull(resultCar);
        Assert.assertThat(resultCar.getNumber(), is(car.getNumber()));
    }

    @Test
    public void testGetAllCars_CorrectSize() {
        //GIVEN
        String carNumber = "carnumber";
        Car car = new Car.Builder(carNumber)
                .type("Audi")
                .name("Q7")
                .quality(Car.QualityClass.B)
                .build();
        List<Car> sourceList = new ArrayList<>();
        sourceList.add(car);
        sourceList.add(car);
        sourceList.add(car);
        for (Car carToAdd : sourceList) {
            carDao.saveCar(carToAdd);
        }

        //WHEN
        List<Car> resultListCar = carDao.getCarsList();

        //THEN
        Assert.assertNotNull(resultListCar);
        //Assert.assertThat(resultListCar.size(), is(sourceList.size()));
    }

    @Test
    public void testDeleteCar_CarNotExist() {
        //GIVEN
        String carNumber = "carnumberToDelete";
        Car car = new Car.Builder(carNumber)
                .type("Audi")
                .name("Q7")
                .quality(Car.QualityClass.B)
                .build();
        carDao.saveCar(car);
        Car carToDelete = carDao.findCar(carNumber);
        carDao.deleteCar(carToDelete);

        //WHEN
        Car resultCar = carDao.findCar(carNumber);

        //THEN
        Assert.assertNull(resultCar);
    }

}
