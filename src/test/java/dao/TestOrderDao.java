package dao;

import com.epam.rd.summer2019.dao.UserDao;
import com.epam.rd.summer2019.dao.impl.CarDaoImpl;
import com.epam.rd.summer2019.dao.impl.OrderDaoImpl;
import com.epam.rd.summer2019.dao.impl.UserDaoImpl;
import com.epam.rd.summer2019.dao.utils.ConnectionH2;
import com.epam.rd.summer2019.domain.Car;
import com.epam.rd.summer2019.domain.Order;
import com.epam.rd.summer2019.domain.User;
import org.junit.*;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;

public class TestOrderDao {

    private CarDaoImpl carDao;
    private OrderDaoImpl orderDao;
    private UserDao userDao;
    private ConnectionH2 connectionUtils;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void init(){
        connectionUtils = new ConnectionH2();
        connectionUtils.prepareDb();
        orderDao = new OrderDaoImpl(connectionUtils);
        carDao = new CarDaoImpl(connectionUtils);
        userDao = new UserDaoImpl(connectionUtils);
    }

    @After
    public void tearDown(){
        List<Order> orderList = orderDao.getOrderList();
        for (Order order: orderList) orderDao.deleteOrder(order);
        connectionUtils.stopDb();
        orderDao = null;
        carDao = null;
        userDao = null;
        connectionUtils = null;
    }

    @Test
    public void testCreateOrder_NotNull_CorrectId() {
        //GIVEN
        String carNumber = "carNumber";
        Car car = new Car.Builder(carNumber).quality(Car.QualityClass.B).build();
        carDao.saveCar(car);
        car = carDao.findCar(carNumber);

        User user = new User(1,"firstName","lastName","userName","password","email",User.Role.CLIENT, User.UserStatus.ACTIVE);
        userDao.saveUser(user);

        String orderCode = "orderCode";
        Order order = new Order.Builder(orderCode,"WE2233", car)
                .status(Order.Status.APPROVED)
                .user(user)
                .build();
        orderDao.saveOrder(order);

        //WHEN
        Order resultOrder = orderDao.findOrder(orderCode);

        //THEN
        Assert.assertNotNull(resultOrder);
        Assert.assertThat(resultOrder.getCode(), is(order.getCode()));
    }

    @Test
    public void testModifyOrder_NotNull_ChangeData() {
        //GIVEN
        String carNumber = "carNumber";
        Car car = new Car.Builder(carNumber).quality(Car.QualityClass.B).build();
        carDao.saveCar(car);
        car = carDao.findCar(carNumber);

        User user = new User(1,"firstName","lastName","userName","password","email",User.Role.CLIENT, User.UserStatus.ACTIVE);
        userDao.saveUser(user);

        String orderCode = "orderCode";
        String orderCodeNew = "orderCodeNew";
        Order order = new Order.Builder(orderCode,"WE2233", car)
                .status(Order.Status.APPROVED)
                .user(user)
                .build();
        orderDao.saveOrder(order);
        order.setCode(orderCodeNew);
        orderDao.saveOrder(order);

        //WHEN
        Order resultOrder = orderDao.findOrder(orderCodeNew);

        //THEN
        Assert.assertNotNull(resultOrder);
        Assert.assertThat(resultOrder.getCode(), is(order.getCode()));
    }

    @Test
    public void testGetAllOrders_CorrectSize() {
        //GIVEN
        String carNumber = "carNumber";
        Car car = new Car.Builder(carNumber).quality(Car.QualityClass.B).build();
        carDao.saveCar(car);
        car = carDao.findCar(carNumber);

        User user = new User(1,"firstName","lastName","userName","password","email",User.Role.CLIENT, User.UserStatus.ACTIVE);
        userDao.saveUser(user);

        String orderCode = "orderCode";
        Order order = new Order.Builder(orderCode,"WE2233", car)
                .status(Order.Status.APPROVED)
                .user(user)
                .build();
        List<Order> sourceList = new ArrayList<>();
        sourceList.add(order);
        sourceList.add(order);
        sourceList.add(order);
        for (Order orderToAdd : sourceList) {
            orderDao.saveOrder(orderToAdd);
        }
        int expectedSize = sourceList.size();

        //WHEN
        List<Order> resultListOrder = orderDao.getOrderList();

        //THEN
        Assert.assertNotNull(resultListOrder);
        Assert.assertThat(resultListOrder.size(), is(expectedSize));
    }

    @Test
    public void testDeleteOrder_OrderNotExist() {
        //GIVEN
        String carNumber = "carNumber";
        Car car = new Car.Builder(carNumber).quality(Car.QualityClass.B).build();
        carDao.saveCar(car);
        car = carDao.findCar(carNumber);

        User user = new User(1,"firstName","lastName","userName","password","email",User.Role.CLIENT, User.UserStatus.ACTIVE);
        userDao.saveUser(user);

        String orderCode = "orderCode";
        Order order = new Order.Builder(orderCode,"WE2233", car)
                .status(Order.Status.APPROVED)
                .user(user)
                .build();
        orderDao.saveOrder(order);
        order = orderDao.findOrder(orderCode);
        orderDao.deleteOrder(order);

        //WHEN
        Order resultOrder = orderDao.findOrder(orderCode);

        //THEN
        Assert.assertNull(resultOrder);
    }
}
