package services;

import com.epam.rd.summer2019.domain.Order;
import com.epam.rd.summer2019.dto.CarViewDTO;
import com.epam.rd.summer2019.dto.OrderViewDTO;
import com.epam.rd.summer2019.services.CarService;
import com.epam.rd.summer2019.services.UserService;
import com.epam.rd.summer2019.services.statuses.CarStatusService;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.Matchers.is;

@RunWith(Parameterized.class)
public class TestCarStatus {

    private CarStatusService carStatusService;

    private CarService mockCarService;
    private UserService mockUserService;
    private CarViewDTO carViewDTO;
    private OrderViewDTO orderViewDTO;

    private Order.Status orderStatus;
    private boolean carStatus;
    private boolean expectedCarStatus;

    @Before
    public void init() {
        mockCarService = Mockito.mock(CarService.class);
        mockUserService = Mockito.mock(UserService.class);
        carStatusService = new CarStatusService(mockCarService);
        carViewDTO = new CarViewDTO();
        orderViewDTO = new OrderViewDTO(mockCarService, mockUserService);
    }


    public TestCarStatus(Boolean carStatus, Order.Status orderStatus, boolean expectedCarStatus) {
        this.carStatus = carStatus;
        this.orderStatus = orderStatus;
        this.expectedCarStatus = expectedCarStatus;
    }

    @Parameterized.Parameters
    public static Collection carStatuses() {
        return Arrays.asList(new Object[][]{
                //status should changed to false
                {false, Order.Status.NEW, false},
                {true, Order.Status.NEW, false},
                //status should be changed to true
                {false, Order.Status.REJECTED, true},
                {true, Order.Status.REJECTED, true},
                {false, Order.Status.CLOSED, true},
                {true, Order.Status.CLOSED, true},
                //no status changes
                {true, Order.Status.APPROVED, true},
                {false, Order.Status.APPROVED, false},
                {true, Order.Status.ISSUE, true},
                {false, Order.Status.ISSUE, false},
                {true, Order.Status.PAYED, true},
                {false, Order.Status.PAYED, false}
        });
    }

    @After
    public void tearDown() {

    }

    @Test
    public void testCarStatusChanged_NotAvailable_param() {

        //GIVEN
        carViewDTO.setAvailable(carStatus);
        orderViewDTO.setCar(carViewDTO);
        orderViewDTO.setStatus(orderStatus);

        Mockito.doNothing().when(mockCarService).modifyCar(carViewDTO);
        carStatusService.setCarStatusService(mockCarService);

        //WHEN
        carStatusService.changeCarStatus(orderViewDTO);

        //THEN
        Assert.assertThat(carViewDTO.isAvailable(), is(expectedCarStatus));

    }
}
