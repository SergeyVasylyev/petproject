package services;

import com.epam.rd.summer2019.services.calc.CalculationService;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

import org.junit.*;
import javax.servlet.http.HttpServletRequest;
import static org.hamcrest.Matchers.is;

public class TestCalculation {

    private CalculationService calculationService;
    private HttpServletRequest mockRequest;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void init(){
        calculationService = new CalculationService();
        mockRequest = Mockito.mock(HttpServletRequest.class);
    }

    @After
    public void tearDown(){}

    @Test
    public void testSumCalculation_WithDriverIncluded(){
        //GIVEN
        String days = "1";
        String rentPrice = "10";
        String driverIncluded = "on";
        Double expectedSum = calculationService.DRIVER_INCLUDED_SUM + Double.parseDouble(rentPrice) * Integer.parseInt(days);

        Mockito.when(mockRequest.getParameter("days")).thenReturn(days);
        Mockito.when(mockRequest.getParameter("rentPrice")).thenReturn(rentPrice);
        Mockito.when(mockRequest.getParameter("driverIncluded")).thenReturn(driverIncluded);

        //WHEN
        Double resultSum = calculationService.getSum(mockRequest);

        //THEN
        Assert.assertNotNull(resultSum);
        Assert.assertThat(resultSum, is(expectedSum));
    }

    @Test
    public void testSumCalculation_WithoutDriverIncluded(){
        //GIVEN
        String days = "1";
        String rentPrice = "10";
        String driverIncluded = null;
        Double expectedSum = Double.parseDouble(rentPrice) * Integer.parseInt(days);

        Mockito.when(mockRequest.getParameter("days")).thenReturn(days);
        Mockito.when(mockRequest.getParameter("rentPrice")).thenReturn(rentPrice);
        Mockito.when(mockRequest.getParameter("driverIncluded")).thenReturn(driverIncluded);

        //WHEN
        Double resultSum = calculationService.getSum(mockRequest);

        //THEN
        Assert.assertNotNull(resultSum);
        Assert.assertThat(resultSum, is(expectedSum));
    }

}
