package services;

import com.epam.rd.summer2019.domain.Order;
import com.epam.rd.summer2019.dto.CarViewDTO;
import com.epam.rd.summer2019.dto.OrderViewDTO;
import com.epam.rd.summer2019.services.CarService;
import com.epam.rd.summer2019.services.OrderService;
import com.epam.rd.summer2019.services.UserService;
import com.epam.rd.summer2019.services.statuses.CarStatusService;
import com.epam.rd.summer2019.services.statuses.OrderStatusService;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collection;

import static com.epam.rd.summer2019.domain.Order.Status.*;
import static org.hamcrest.Matchers.is;

@RunWith(Parameterized.class)
public class TestOrderStatus {

    private OrderStatusService orderStatusService;
    private CarStatusService mockCarStatusService;
    private OrderService mockOrderService;
    private CarService mockCarService;
    private UserService mockUserService;
    private CarViewDTO carViewDTO;
    private OrderViewDTO orderViewDTO;

    private Order.Status orderStatus;
    private String actionButton;
    private Order.Status expectedOrderStatus;

    @Before
    public void init() {
        mockCarService = Mockito.mock(CarService.class);
        mockUserService = Mockito.mock(UserService.class);
        mockCarStatusService = Mockito.mock(CarStatusService.class);
        mockOrderService = Mockito.mock(OrderService.class);
        orderStatusService = new OrderStatusService(mockOrderService, mockCarStatusService);
        carViewDTO = new CarViewDTO();
        orderViewDTO = new OrderViewDTO(mockCarService, mockUserService);
    }

    public TestOrderStatus(Order.Status orderStatus, String actionButton ,Order.Status expectedOrderStatus) {
        this.orderStatus = orderStatus;
        this.actionButton = actionButton;
        this.expectedOrderStatus = expectedOrderStatus;
    }

    @Parameterized.Parameters
    public static Collection orderStatuses() {
        return Arrays.asList(new Object[][]{
                //status should be changed
                {NEW, "Reject", REJECTED},
                {NEW, "any string", APPROVED},
                {NEW, "", APPROVED},
                {APPROVED, "", PAYED},
                {PAYED, "Issue", ISSUE},
                {PAYED, "any string", CLOSED},
                {PAYED, "", CLOSED},
                {ISSUE, "", CLOSED}
        });
    }

    @After
    public void tearDown() {

    }

    @Test
    public void testOrderStatusChanged_param() {
        //GIVEN
        orderViewDTO.setCar(carViewDTO);
        orderViewDTO.setStatus(orderStatus);

        Mockito.doNothing().when(mockCarStatusService).changeCarStatus(orderViewDTO);
        Mockito.doNothing().when(mockOrderService).modifyOrder(orderViewDTO);
        orderStatusService.setCarStatusService(mockCarStatusService);
        orderStatusService.setOrderService(mockOrderService);

        //WHEN
        orderStatusService.changeOrderStatus(orderViewDTO, actionButton);

        //THEN
        Assert.assertThat(orderViewDTO.getStatus(), is(expectedOrderStatus));
    }

}
