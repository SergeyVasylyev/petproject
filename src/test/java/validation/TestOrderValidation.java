package validation;

import com.epam.rd.summer2019.validation.impl.OrderValidationImpl;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;

import static org.hamcrest.Matchers.is;

public class TestOrderValidation {

    private HttpServletRequest mockRequest;
    private String wrongPassport = "ValidationPassport";
    private String wrongDays = "ValidationDays";
    private OrderValidationImpl orderValidation;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void init() {
        orderValidation = new OrderValidationImpl();
        mockRequest = Mockito.mock(HttpServletRequest.class);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testValidation_valid() {
        //GIVEN
        String passport = "SS123123";
        String days = "2";

        Mockito.when(mockRequest.getParameter("passport")).thenReturn(passport);
        Mockito.when(mockRequest.getParameter("days")).thenReturn(days);

        //WHEN
        String validationResult = orderValidation.validate(mockRequest);

        //THEN
        Assert.assertNotNull(validationResult);
        Assert.assertTrue(validationResult.isEmpty());
    }

    @Test
    public void testValidation_invalidPassport() {
        //GIVEN
        String passport = "SS-123123";
        String days = "2";

        Mockito.when(mockRequest.getParameter("passport")).thenReturn(passport);
        Mockito.when(mockRequest.getParameter("days")).thenReturn(days);

        //WHEN
        String validationResult = orderValidation.validate(mockRequest);

        //THEN
        Assert.assertNotNull(validationResult);
        Assert.assertThat(validationResult, is(wrongPassport));
    }

    @Test
    public void testValidation_invalidDays() {
        //GIVEN
        String passport = "SS123123";
        String days = "-2";

        Mockito.when(mockRequest.getParameter("passport")).thenReturn(passport);
        Mockito.when(mockRequest.getParameter("days")).thenReturn(days);

        //WHEN
        String validationResult = orderValidation.validate(mockRequest);

        //THEN
        Assert.assertNotNull(validationResult);
        Assert.assertThat(validationResult, is(wrongDays));
    }

    @Test
    public void testValidation_invalidPassportDays() {
        //GIVEN
        String passport = "SS3123";
        String days = "-2";

        Mockito.when(mockRequest.getParameter("passport")).thenReturn(passport);
        Mockito.when(mockRequest.getParameter("days")).thenReturn(days);

        //WHEN
        String validationResult = orderValidation.validate(mockRequest);

        //THEN
        Assert.assertNotNull(validationResult);
        Assert.assertThat(validationResult, is(wrongPassport + wrongDays));
    }

}
