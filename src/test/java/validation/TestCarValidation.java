package validation;

import com.epam.rd.summer2019.validation.impl.CarValidationImpl;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;

import static org.hamcrest.Matchers.is;

public class TestCarValidation {

    private HttpServletRequest mockRequest;
    private String wrongQuality = "ValidateQuality";
    private String wrongPrice = "ValidateRentPrice";
    private CarValidationImpl carValidation;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void init() {
        carValidation = new CarValidationImpl();
        mockRequest = Mockito.mock(HttpServletRequest.class);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testValidation_valid() {
        //GIVEN
        String quality = "A";
        String rentPrice = "20";

        Mockito.when(mockRequest.getParameter("quality")).thenReturn(quality);
        Mockito.when(mockRequest.getParameter("rentPrice")).thenReturn(rentPrice);

        //WHEN
        String validationResult = carValidation.validate(mockRequest);

        //THEN
        Assert.assertNotNull(validationResult);
        Assert.assertTrue(validationResult.isEmpty());
    }

    @Test
    public void testValidation_invalidQuality() {
        //GIVEN
        String quality = "A2";
        String rentPrice = "20";

        Mockito.when(mockRequest.getParameter("quality")).thenReturn(quality);
        Mockito.when(mockRequest.getParameter("rentPrice")).thenReturn(rentPrice);

        //WHEN
        String validationResult = carValidation.validate(mockRequest);

        //THEN
        Assert.assertNotNull(validationResult);
        Assert.assertThat(validationResult, is(wrongQuality));
    }

    @Test
    public void testValidation_invalidRentPrice() {
        //GIVEN
        String quality = "A";
        String rentPrice = "-20";

        Mockito.when(mockRequest.getParameter("quality")).thenReturn(quality);
        Mockito.when(mockRequest.getParameter("rentPrice")).thenReturn(rentPrice);

        //WHEN
        String validationResult = carValidation.validate(mockRequest);

        //THEN
        Assert.assertNotNull(validationResult);
        Assert.assertThat(validationResult, is(wrongPrice));
    }

    @Test
    public void testValidation_invalidQualityRentPrice() {
        //GIVEN
        String quality = "A2";
        String rentPrice = "-20";

        Mockito.when(mockRequest.getParameter("quality")).thenReturn(quality);
        Mockito.when(mockRequest.getParameter("rentPrice")).thenReturn(rentPrice);

        //WHEN
        String validationResult = carValidation.validate(mockRequest);

        //THEN
        Assert.assertNotNull(validationResult);
        Assert.assertThat(validationResult, is(wrongQuality + wrongPrice));
    }

}
