package validation;

import com.epam.rd.summer2019.validation.impl.InvoiceValidationImpl;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;

import static org.hamcrest.Matchers.is;

public class TestInvoiceValidation {

    private HttpServletRequest mockRequest;
    private String wrongSum = "ValidateSum";
    private InvoiceValidationImpl invoiceValidation;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void init() {
        invoiceValidation = new InvoiceValidationImpl();
        mockRequest = Mockito.mock(HttpServletRequest.class);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testValidation_valid() {
        //GIVEN
        String sum = "20.1";

        Mockito.when(mockRequest.getParameter("sum")).thenReturn(sum);

        //WHEN
        String validationResult = invoiceValidation.validate(mockRequest);

        //THEN
        Assert.assertNotNull(validationResult);
        Assert.assertTrue(validationResult.isEmpty());
    }

    @Test
    public void testValidation_invalidSum() {
        //GIVEN
        String sum = "-20.1";

        Mockito.when(mockRequest.getParameter("sum")).thenReturn(sum);

        //WHEN
        String validationResult = invoiceValidation.validate(mockRequest);

        //THEN
        Assert.assertNotNull(validationResult);
        Assert.assertThat(validationResult, is(wrongSum));
    }

}
