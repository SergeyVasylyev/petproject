**Homework_21**

Spring MVC
Exception handler.

added GlobalExceptionHandler.
Handle 2 exceptions, in case of other exception redirect to login page with error message

ApplicationException - for Car, Order, Invoice controllers validation

UserNotFoundException - for login error  
   

**pet project spring**

uses Initializer.java to define AnnotationConfigWebApplicationContext
Spring configuration is in WebAppConfig.java

no changes in logic only Spring framework added for dao + services + converters + controllers

Project task description: resources.pp_14.txt

Common description for user scenarios:
Client:
1. Register and login in the system.
2. Choose car and create rent order 
3. When order checked and approved by manager receive invoice
4. Pay invoice and use car
5. If there were any issue with car - invoice for issue will be created
6. Pay invoice for issue

Manager:
1. Receive rent order
2. Reject rent order or Approve rent order and create invoice 
3. Check car and create invoice for issue if necessary
4. Close rent order
      
